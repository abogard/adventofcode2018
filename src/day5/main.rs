use std::collections::{VecDeque, HashSet};
use utf8_chars::BufReadCharsExt;
use itertools::unfold;

use tools::utils;

const FILE_PATH: &str = "resources/day5/part1.txt";

fn read_chars() -> impl Iterator<Item=char> {
    unfold(
        utils::get_buff_reader(FILE_PATH),
        |reader| reader.read_char().ok().flatten()
    )
}

fn reduce_polymers(chars: impl Iterator<Item=char>) -> usize {
    let mut buffer: VecDeque<char> = VecDeque::new();

    for next_char in chars {
        if let Some(&prev_char) = buffer.front() {
            if prev_char != next_char && prev_char.to_uppercase().eq(next_char.to_uppercase()) {
                buffer.pop_front();
                continue;
            }
        }

        buffer.push_front(next_char)
    }

    buffer.len()
}

fn part1() {
    println!("part 1: {:?}", reduce_polymers(read_chars()))
}

fn part2() {
    let all_chars = read_chars().collect::<HashSet<char>>();
    let min = all_chars
        .iter()
        .map(|omit_char| reduce_polymers(
            read_chars()
                .filter(|char| char.to_uppercase().ne(omit_char.to_uppercase()))
            )
        ).min()
         .unwrap();

    println!("part 2: {:?}", min)
}

fn main() {
    part1();
    part2();
}