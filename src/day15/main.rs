#![feature(bindings_after_at)]
#![feature(map_first_last)]

use std::collections::{HashMap, BTreeMap, VecDeque, BTreeSet, HashSet};
use std::cmp::Ordering;

#[macro_use] extern crate maplit;

use UnitType::{Goblin, Elf};
use tools::geometry::geometry2d::*;
use tools::utils::get_file_contents;

const FILE_PATH: &str = "resources/day15/part1.txt";
const DEFAULT_HP: u8 = 200;

#[derive(PartialOrd, Eq, PartialEq, Hash, Clone, Debug, Copy)]
enum UnitType {
    Elf(u8),
    Goblin(u8)
}

impl UnitType {
    fn is_elf(&self) -> bool {
        if let Elf(_) = self { true } else { false }
    }

    fn is_goblin(&self) -> bool {
        !self.is_elf()
    }

    fn is_enemy(&self, other: &Self) -> bool {
        self.is_elf() ^ other.is_elf()
    }

    fn get_hp(&self) -> &u8 {
        match self {
            Elf(hp) | Goblin(hp) => hp
        }
    }

    fn reduce_hp_by(&self, by: u8) -> Option<Self> {
        match self {
            Elf(hp) if *hp > by => Some(Elf(*hp - by)),
            Goblin(hp) if *hp > by => Some(Goblin(*hp - by)),
            _ => None
        }
    }
}

impl Ord for UnitType {
    fn cmp(&self, other: &Self) -> Ordering {
        self.get_hp().cmp(other.get_hp())
    }
}

type Units = BTreeMap<Point, UnitType>;
type Board = HashMap<Point, bool>;

const DIRECTIONS: [Point;4] = [Point(0, -1), Point(-1, 0), Point(1, 0), Point(0, 1)];

fn read_map(path: &str) -> (Board, Units) {
    let mut map = Board::new();
    let mut units = Units::new();


    for (y, line) in get_file_contents(path).enumerate() {
        for (x, char) in line.chars().enumerate() {
            let pos = Point(x as i64, y as i64);

            map.insert(pos.clone(), char != '#');

            match char {
                'E' => units.insert(pos, Elf(DEFAULT_HP)),
                'G' => units.insert(pos, Goblin(DEFAULT_HP)),
                _   => None
            };
        }
    }

    (map, units)
}

type Direction = Point;
type End = Point;
type Previous = Point;
type Distance = usize;

type Path = (End, Previous, Direction, Distance);
type Enemy = (Distance, Previous, UnitType, Direction, End);

fn find_closest_enemy(
    (attacker_pos, attacker): &(Point, UnitType),
    units: &Units, board: &Board
) -> Option<Enemy> {
    let mut queue = VecDeque::<Path>::new();
    let mut nearest = BTreeSet::<Enemy>::new();
    let mut visited = hashset!{attacker_pos.clone()};

    for &direction in &DIRECTIONS {
        queue.push_back((*attacker_pos + direction, *attacker_pos, direction, 0));
    }

    while let Some((
        current,
        previous,
        start_direction,
        distance
    )) = queue.pop_front() {
        let unit = units.get(&current);

        if !visited.insert(current) {
            continue;
        }

        match unit {
            Some(e) if attacker.is_enemy(e) => {
                nearest.insert((
                    distance,
                    previous,
                    *e,
                    if distance == 0 { Point(0, 0) } else { start_direction },
                    current
                ));
            }, None if *board.get(&current).unwrap_or(&false)
                && nearest
                    .first()
                    .iter()
                    .all(|(shortest, _, _, _, _)| distance < *shortest) =>

                for &direction in &DIRECTIONS {
                    queue.push_back((
                        current + direction,
                        current,
                        start_direction,
                        distance + 1
                    ));
                },
            _ => {}
        }
    }

    nearest.into_iter().next()
}

fn should_play(units: &Units) -> bool {
    units.values().any(UnitType::is_elf) && units.values().any(UnitType::is_goblin)
}

fn play(path: &str, unit_damage: impl Fn(&UnitType) -> u8) -> (usize, Units) {
    let (board, mut units) = read_map(path);
    let mut moved = HashSet::<Point>::new();

    for round in 0usize.. {
        while let Some(attacker@(attacker_pos, unit)) =
            units
                .iter()
                .find(|(&p, _)| !moved.contains(&p))
                .map(|(&p, &u)| (p, u)) {

            if let Some((
                distance,
                attacker_final_pos,
                enemy,
                attacker_move_direction,
                enemy_pos
            )) = find_closest_enemy(&attacker,  &units, &board) {
                units.remove(&attacker_pos);

                if distance <= 1 {
                    units.insert(attacker_final_pos, unit);
                    moved.insert(attacker_final_pos);

                    if let Some(damaged_enemy) = enemy.reduce_hp_by(unit_damage(&unit)) {
                        units.insert(enemy_pos, damaged_enemy);
                    } else {
                        units.remove(&enemy_pos);

                        if !should_play(&units) {
                            let all_moved = units.keys().all(|k| moved.contains(k));

                            return (round + if all_moved { 1 } else { 0 }, units)
                        }
                    }
                } else {
                    units.insert(attacker_pos + attacker_move_direction, unit);
                    moved.insert(attacker_pos + attacker_move_direction);
                }
            } else {
                moved.insert(attacker_pos);
            }
        }

        moved.clear();
    }

    (0, units)
}

fn compute_hp_sum(units: &Units) -> usize {
    units
        .values()
        .map(|u| *u.get_hp() as usize)
        .sum::<usize>()
}

fn play_and_generate_results(path: &str) -> usize {
    let (rounds, units) = play(path, |_| 3);

    rounds * compute_hp_sum(&units)
}

fn part1() {
    let result = play_and_generate_results(FILE_PATH);

    println!("part 1: {}", result);
}

fn part2() {
    let (_, units) = read_map(FILE_PATH);
    let number_of_elves = units
        .values()
            .filter(|unit| unit.is_elf())
            .count();

    for damage in 4u8.. {
        let (rounds, units) = play(
            FILE_PATH,
            |u| if u.is_elf() { damage } else { 3 }
        );

        if units.values().any(|u| u.is_elf() && units.len() == number_of_elves) {
            println!("part 2: {}", rounds * compute_hp_sum(&units));
            return;
        }
    }
}

fn main() {
    part1();
    part2();
}

mod test;