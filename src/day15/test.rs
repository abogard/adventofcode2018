#[allow(unused_imports)]
use super::play_and_generate_results;

#[test]
fn test1() {
    assert_eq!(play_and_generate_results("resources/day15/test1.txt"), 27730)
}

#[test]
fn test2() {
    assert_eq!(play_and_generate_results("resources/day15/test2.txt"), 36334)
}

#[test]
fn test3() {
    assert_eq!(play_and_generate_results("resources/day15/test3.txt"), 39514)
}

#[test]
fn test4() {
    assert_eq!(play_and_generate_results("resources/day15/test4.txt"), 27755)
}

#[test]
fn test5() {
    assert_eq!(play_and_generate_results("resources/day15/test5.txt"), 28944)
}

#[test]
fn test6() {
    assert_eq!(play_and_generate_results("resources/day15/test6.txt"), 18740)
}