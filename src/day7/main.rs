#![feature(bindings_after_at)]

use regex::Regex;
use itertools::{unfold, Itertools};
use std::collections::{BTreeMap, HashSet, BinaryHeap};
use std::cmp::Reverse;
use once_cell::sync::Lazy;

use macros::regex;
use tools::utils::get_file_contents;

const FILE_PATH: &str = "resources/day7/part1.txt";
const DEPENDENCY_REGEX: Lazy<Regex> =
    regex!(r"Step (.) must be finished before step (.) can begin\.");

type Job = char;
type PrecedingJobs = HashSet<Job>;
type Graph = BTreeMap<Job, PrecedingJobs>;

fn read_graph() -> Graph  {
    let lines = get_file_contents(FILE_PATH);
    let connections = lines.map(|line|{
        let capture = DEPENDENCY_REGEX
            .captures(line.as_str())
            .expect(&format!("{} doesn't match regex {}", line, DEPENDENCY_REGEX.as_str()));

        (capture[1].parse::<Job>().unwrap(), capture[2].parse::<Job>().unwrap())
    });

    let mut graph = Graph::new();

    for (before, after) in connections {
        graph
            .entry(after)
            .or_default()
            .insert(before);

        graph
            .entry(before)
            .or_default();
    }

    graph
}

fn part1() {
    let graph = read_graph();
    let result = unfold(graph, |g|{
        let ready_node = g
            .iter()
            .find_map(|(n, p)| if p.is_empty() { Some(*n) } else { None });

        for ready in ready_node {
            g.remove(&ready);
            for (_, predecessors) in g.iter_mut() {
                predecessors.remove(&ready);
            }
        }

        ready_node
    }).collect::<String>();

    println!("part 1: {:?}", result);
}

type Time = u16;
type OngoingJob = Reverse<(Time, Job)>;
type OngoingJobs = Vec<OngoingJob>;
type JobsQueue = BinaryHeap<OngoingJob>;

fn part2() {
    let graph = read_graph();
    let mut workers = 5usize;
    let mut working = JobsQueue::with_capacity(workers);
    let mut finished = HashSet::<Job>::new();

    let result: u16 = unfold(graph, |g|{
        let min_time = if let Some(&Reverse((min_time, _))) = working.peek() {
            for Reverse((time, job)) in working.drain().collect::<OngoingJobs>() {
                let diff = time - min_time;

                if diff < 1 {
                    finished.insert(job);
                    workers += 1;
                } else {
                    working.push(Reverse((diff, job)))
                }
            }
            min_time
        } else { 0 };

        for finished_job in finished.drain() {
            for (_, preceding) in g.iter_mut() {
                preceding.remove(&finished_job);
            }
        }

        let started = g
            .iter()
            .filter(|(_, preceding)| preceding.is_empty())
            .map(|(&job,_)| (job as Time - 'A' as Time + 61, job))
            .take(workers);

        for item@(_, job) in started.collect_vec() {
            g.remove(&job);
            working.push(Reverse(item));

            workers -= 1;
        }

        if working.is_empty() && min_time == 0 { None } else { Some(min_time) }
    }).sum();

    println!("part 2: {:?}", result);
}

fn main() {
    part1();
    part2()
}