use itertools::{unfold, Itertools};

const INPUT: &'static str = "084601";
const START_RECIPES_COUNT: usize = 84601;
const END_RECIPES_COUNT: usize = 84601 + 10;

fn split_into_numbers(number: u64) -> impl Iterator<Item=u8> {
    unfold((number, true), |(num, initial)|
        if *num > 0 || *initial {
            let result = (*num % 10) as u8;
            *num /= 10;
            *initial = false;

            Some(result)
        } else {
            None
        })
}

fn calculate_recipes(continue_condition: impl Fn(&Vec<u8>) -> bool) -> Vec<u8> {
    let mut elf1_pos = 0usize;
    let mut elf2_pos = 1usize;
    let mut recipes = vec![3u8, 7u8];

    while continue_condition(&recipes) {
        let new_recipes = recipes[elf1_pos] as u64 + recipes[elf2_pos] as u64;
        let mut new_recipes_vec = split_into_numbers(new_recipes).collect_vec();

        new_recipes_vec.reverse();

        recipes.append(&mut new_recipes_vec);

        elf1_pos = (elf1_pos + (1 + recipes[elf1_pos] as usize)) % recipes.len();
        elf2_pos = (elf2_pos + (1 + recipes[elf2_pos] as usize)) % recipes.len();
    }

    recipes
}


fn part1() {
    let recipes =  calculate_recipes(|v| v.len() < END_RECIPES_COUNT);
    let result =
        &recipes[START_RECIPES_COUNT..END_RECIPES_COUNT]
            .iter()
            .map(u8::to_string)
            .join("");

    println!("part 1: {}", result);
}

fn is_last(input: &Vec<u8>, result: &Vec<u8>) -> bool {
    result.len() >= input.len() && &result[result.len() - input.len()..result.len()] == &input[..]
}

fn is_one_before_last(input: &Vec<u8>, result: &Vec<u8>) -> bool {
    result.len() >= input.len() + 1 &&
   &result[result.len() - input.len() - 1..result.len() - 1] == &input[..]
}

fn part2() {
    let input =
        INPUT
            .chars()
            .into_iter()
            .flat_map(|c| c.to_digit(10))
            .map(|c| c as u8)
            .collect_vec();

    let recipes =  calculate_recipes(|v|
        !is_last(&input, v) && !is_one_before_last(&input, v));

    let offset = if is_one_before_last(&input, &recipes) { 1 } else { 0 };

    println!("part 2: {}", recipes.len() - input.len() - offset);
}

fn main() {
    part1();
    part2();
}