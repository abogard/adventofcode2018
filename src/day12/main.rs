#![feature(map_first_last)]

use std::collections::HashMap;
use regex::Regex;
use std::error::Error;
use itertools::Itertools;
use once_cell::sync::Lazy;

use macros::regex;
use tools::utils::{get_file_contents, results::prepend};

const FILE_PATH: &'static str = "resources/day12/part1.txt";
const DEFAULT_FIELD: char = '.';
const PATTERN_LENGTH: usize = 5;
const PATTERN_OFFSET: usize = 2;
const INITIAL_REGEX: Lazy<Regex> = regex!(r"^initial state: ([.#]+)");
const PATTERN_REGEX: Lazy<Regex> = regex!(r"^([.#]{5}) => ([.#])");

type Mutation = char;
type Pattern = String;
type InitialPattern = String;

fn read_input() -> Result<(InitialPattern, HashMap<Pattern, Mutation>), Box<dyn Error>> {
    let mut lines = get_file_contents(FILE_PATH);
    let initial_plants = lines
        .find_map(|line| INITIAL_REGEX
                .captures(&line)
                .map(|capture| capture[1].to_string())
        ).ok_or("Initial pattern couldn't be found!")?;

    let plants_transitions = lines
        .flat_map(|str|
            PATTERN_REGEX
                .captures_iter(&str)
                .map(|capture| (capture[1].to_string(), capture[2].to_string()))
                .collect_vec()
         );

    let as_map =
        plants_transitions
            .map(|(pattern, growth)| prepend(growth.parse::<Mutation>(), pattern))
            .collect::<Result<HashMap<_, _>, _>>()?;

    Ok((initial_plants, as_map))
}

fn compute_sum(iterations: usize) -> i64 {
    let (initial, patterns) = read_input().unwrap();

    let fill = DEFAULT_FIELD.to_string().repeat(PATTERN_LENGTH - 1);
    let redundant =  DEFAULT_FIELD.to_string().repeat(PATTERN_LENGTH);
    let default_field = DEFAULT_FIELD.to_string();

    let mut current = initial;
    let mut previous = String::from("");
    let mut start_offset = 0i64;
    let mut prev_start_offset = 0i64;

    for i in 0..iterations {
        while &current[0..5] == redundant {
            current = current.split_off(1);
            start_offset += 1;
        }

        while &current[0..4] != fill {
            current = default_field.clone() + &current;
            start_offset -= 1;
        }

        while &current[current.len() - PATTERN_LENGTH + 1..current.len()] != fill {
            current.push_str(&default_field)
        }

        if current == previous {
            let diff = start_offset - prev_start_offset;
            let result = (iterations - i) as i64 * diff;

            start_offset += result;

            break;
        }

        let mut next= String::from(&current[0..PATTERN_OFFSET]);

        previous = current.clone();
        prev_start_offset = start_offset;

        for i in 0..current.len() - PATTERN_LENGTH + 1 {
            next +=
                patterns
                    .get(&current[i..i + PATTERN_LENGTH])
                    .map(|&c| c.to_string())
                    .unwrap_or(default_field.clone())
                    .as_str();
        }

        current = next
    }

    current
        .chars()
        .enumerate()
        .filter(|(_, value)| *value != DEFAULT_FIELD)
        .map(|(index, _)| index as i64 + start_offset)
        .sum::<i64>()
}

fn main() {
    println!("part 1: {}", compute_sum(20));
    println!("part 2: {}", compute_sum(5_000_000_000));
}