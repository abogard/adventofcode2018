use std::collections::HashMap;
use itertools::Itertools;

use tools::utils::get_file_contents;

const FILE_PATH: &str = "resources/day2/part1.txt";

fn count_chars(line: String) -> (u64, u64) {
    let collected =
        line
            .chars()
            .fold(HashMap::<char,u64>::new(), |mut acc, c| {
                acc.insert(c, acc.get(&c).unwrap_or(&0) + 1);
                acc
            });

    let has_double = collected.iter().any(|(_, v)| *v == 2);
    let has_triple = collected.iter().any(|(_, v)| *v == 3);

    (if has_double {1} else {0}, if has_triple {1} else {0})
}

fn part1() {
    let (doubles, triples): (u64, u64) =
        get_file_contents(FILE_PATH)
            .map(count_chars)
            .fold((0, 0), |(acc_d, acc_t), (d, t)| (acc_d + d, acc_t + t));

    println!("Part 1: {:?}", doubles * triples)
}

fn compare_codes(left: &String, right: &String) -> Option<String> {
    let comp = left
        .chars()
        .zip(right.chars())
        .filter(|(l,r)| l == r)
        .map(|(l, _)| l)
        .collect::<String>();

    if comp.len() == left.len() - 1 { Some(comp) } else { None }
}

fn part2() {
    let codes = get_file_contents(FILE_PATH).collect_vec();

    for (i, current) in codes.iter().enumerate()  {
        if let Some(matched) = codes[(i + 1)..codes.len()]
            .iter()
            .flat_map(|next|compare_codes(current, next))
            .next() {

            println!("Part 2: {:?}", matched);
            return;
        }
    }

    println!("Matching elements haven't been found!");
}

fn main() {
    part1();
    part2();
}
