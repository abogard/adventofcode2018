#![feature(linked_list_cursors)]

use regex::Regex;
use once_cell::sync::Lazy;
use std::collections::LinkedList;

use macros::regex;
use tools::utils::get_file_contents;

const FILE_PATH_1: &str = "resources/day9/part1.txt";
const FILE_PATH_2: &str = "resources/day9/part2.txt";
const MARBLE_REGEX: Lazy<Regex> = regex!(r"(\d+) players; last marble is worth (\d+) points");

type NumberOfPlayers = usize;
type MarbleScore = usize;

fn read_rules(file: &str) -> Option<(NumberOfPlayers, MarbleScore)> {
    let contents = get_file_contents(file).next()?;
    let capture = MARBLE_REGEX.captures(&contents)?;

    Some((capture[1].parse::<NumberOfPlayers>().ok()?, capture[2].parse::<MarbleScore>().ok()?))
}

fn play(players: NumberOfPlayers, marbles: MarbleScore) -> Option<MarbleScore> {
    let mut scores = vec![0 as MarbleScore; players];
    let mut board = LinkedList::<MarbleScore>::new();
    let mut cursor = board.cursor_front_mut();
    let scores_len = scores.len();

    cursor.insert_after(0);

    for marble in 1..marbles+1 {
        if marble % 23 == 0 {
            for _ in 0..7 {
                if cursor.peek_prev().is_none() {
                    cursor = board.cursor_back_mut();
                } else {
                    cursor.move_prev();
                }
            }
            scores[marble % scores_len] += marble + cursor.remove_current()?;
        } else {
            if cursor.peek_next().is_none() {
                cursor = board.cursor_front_mut();
            } else {
                cursor.move_next();
            }

            cursor.insert_after(marble);
            cursor.move_next();
        }
    }

    Some(scores.into_iter().max().unwrap_or(0))
}

fn part1() {
    let (players, marbles) = read_rules(FILE_PATH_1).unwrap();
    let scores = play(players, marbles);

    println!("part 1: {:?}", scores)
}

fn part2() {
    let (players, marbles) = read_rules(FILE_PATH_2).unwrap();
    let scores = play(players, marbles);

    println!("part 2: {:?}", scores)
}

fn main() {
    part1();
    part2();
}