#![feature(bindings_after_at)]

use regex::Regex;
use std::collections::HashMap;
use once_cell::sync::Lazy;
use reduce::Reduce;

use macros::regex;
use tools::utils::get_file_contents;

const FILE_PATH: &str = "resources/day6/part1.txt";
const POINT_REGEX: Lazy<Regex> = regex!(r"(\d+), (\d+)");

type X = i64;
type Y = i64;
type Point = (X, Y);
type Points = Vec<(X, Y)>;
type PointId = usize;

fn get_points() -> impl Iterator<Item=Point> {
    get_file_contents(FILE_PATH)
        .map(move |str| {
            let capture = POINT_REGEX
                .captures(str.as_str())
                .expect(&format!("Couldn't parse {} into number, number", str));

            (capture[1].parse::<X>().unwrap(), capture[2].parse::<Y>().unwrap())
        })
}

fn get_normalized() -> impl Iterator<Item=Point> {
    let points = get_points().collect::<Points>();
    let &min_x =  points.iter().map(|(x, _)| x).min().unwrap();
    let &min_y =  points.iter().map(|(_, y)| y).min().unwrap();

    points
        .into_iter()
        .map(move |(x, y)| (x - min_x, y - min_y))
}

type Id = usize;
type Distance = i64;
type Repetitions = usize;
type ItemDistance = (Id, Distance, Repetitions);

fn choose_nearest(l@(id1, d1, r1): ItemDistance, r@(_, d2, r2): ItemDistance) -> ItemDistance {
    if d1 < d2 {
        l
    } else if d2 < d1 {
        r
    } else {
        (id1, d1, r1 + r2)
    }
}

fn part1() {
    let points = get_normalized().collect::<Points>();
    let &max_x = points.iter().map(|(x, _)| x).max().unwrap();
    let &max_y = points.iter().map(|(_, y)| y).max().unwrap();

    let mut area = HashMap::<PointId, Points>::new();

    for y2 in 0i64..max_y + 1 {
        for x2 in 0i64..max_x + 1 {
            if let Some((id, _, 1)) = points
                .iter()
                .enumerate()
                .map(|(id, (x1, y1))| (id, (*x1 - x2).abs() + (*y1 - y2).abs(), 1usize))
                .reduce(choose_nearest) {

                area
                    .entry(id)
                    .or_default()
                    .push(points[id])
            }
        }
    }

    let is_unbounded = |&(x, y): &Point| x == 0 || x == max_x || y == 0 || y == max_y;
    let max_area = area
        .values()
        .filter(|v| !v.iter().any(is_unbounded))
        .map(|v| v.len())
        .max()
        .unwrap();

    println!("part 1: {:?}", max_area);
}

fn part2() {
    let points = get_normalized().collect::<Points>();
    let &max_x = points.iter().map(|(x, _)| x).max().unwrap();
    let &max_y = points.iter().map(|(_, y)| y).max().unwrap();

    let mut area= Vec::<Vec<Distance>>::new();

    for y2 in 0..max_y + 1 {
        let mut row = Vec::<Distance>::new();

        for x2 in 0..max_x + 1 {
            let sum = points
                .iter()
                .map(|&(x1, y1)| (x1 - x2).abs() + (y1 - y2).abs())
                .sum::<Distance>();

            row.push(sum);
        }
        area.push(row);
    }

    let area_size =
        area
            .iter()
            .flatten()
            .filter(|&&d| d < 10000)
            .count();

    println!("part 2: {:?}", area_size);
}

fn main() {
    part1();
    part2();
}