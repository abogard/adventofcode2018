#![feature(map_first_last)]

use regex::Regex;
use once_cell::sync::Lazy;
use itertools::Itertools;
use nalgebra::Vector4;
use std::collections::{HashMap, HashSet};

use macros::regex;
use tools::utils::get_file_contents;

const FILE_PATH: &str = "resources/day25/part1.txt";
const SPACE_REGEX: Lazy<Regex> = regex!(r"(-?\d+),(-?\d+),(-?\d+),(-?\d+)");

type IPoint = Vector4<i64>;

fn parse_input() -> Vec<IPoint> {
    get_file_contents(FILE_PATH)
        .map(|l| Vector4::from_iterator(
            SPACE_REGEX
                .captures(&l)
                .unwrap()
                .iter()
                .flatten()
                .skip(1)
                .map(|m| m.as_str().parse::<i64>().unwrap()))
        ).collect_vec()
}

fn manhattan_distance(l: &IPoint, r: &IPoint) -> i64 {
    let diff = r - l;

    diff.iter().map(|coord| coord.abs()).sum()
}

type Graph = HashMap<IPoint, HashSet<IPoint>>;

fn build_graph<'a>() -> Graph {
    let parsed = parse_input();
    let mut graph = Graph::new();

    for i in 0..parsed.len() {
        let current = &parsed[i];

        for j in i..parsed.len() {
            let next = &parsed[j];

            if manhattan_distance(current, next) <= 3 {
                graph
                    .entry(current.clone())
                    .or_insert(HashSet::new())
                    .insert(next.clone());
                graph
                    .entry(next.clone())
                    .or_insert(HashSet::new())
                    .insert(current.clone());
            }
        }
    }

    graph
}

fn connected_components(graph: &Graph) -> usize {
    deep_first_search(graph)
}

fn deep_first_search_impl<'a>(node: IPoint, graph: &'a Graph, to_visit: &mut HashSet<&'a IPoint>) {
    if to_visit.remove(&node) {
        for child in &graph[&node] {
            deep_first_search_impl(child.clone_owned(), graph, to_visit)
        }
    }
}

fn deep_first_search(graph: &Graph) -> usize {
    let mut components = 0usize;
    let mut to_visit = graph.keys().collect::<HashSet<_>>();
    while let Some(node) = to_visit.iter().next() {
        components += 1;

        deep_first_search_impl(node.clone_owned(), graph, &mut to_visit);
    }

    components
}

fn main() {
    println!("part 1: {}", connected_components(&build_graph()));
}