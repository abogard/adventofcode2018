use regex::Regex;
use itertools::{Itertools, MinMaxResult::MinMax};
use std::collections::HashSet;
use once_cell::sync::Lazy;
use if_chain::if_chain;

use macros::regex;
use tools::utils::get_file_contents;

const FILE_PATH: &'static str = "resources/day10/part1.txt";
const OBJECT_REGEX: Lazy<Regex> =
    regex!(r"position=<([- ]\d+), ([- ]\d+)> velocity=<([- ]\d+), ([- ]\d+)>");

type Position = (i32, i32);
type Velocity = (i32, i32);

fn parse_line(line: String) -> Option<Vec<i32>> {
    let capture = OBJECT_REGEX.captures(&line)?;

    capture
        .iter()
        .skip(1)
        .flatten()
        .map(|elem| elem.as_str().trim().parse::<i32>())
        .collect::<Result<Vec<_>, _>>()
        .ok()
}

fn read_data() -> impl Iterator<Item=(Position, Velocity)>{
    get_file_contents(FILE_PATH)
        .map(parse_line)
        .map(|matches|
            if_chain!(
                if let Some(results) = matches;
                if let [x, y, xs, ys] = results[..];
            then {
                ((x, y), (xs, ys))
            } else {
                panic!("Wrong data!")
            })
        )
}

fn print_board(coords: &HashSet<Position>, min_x: i32, max_x: i32, min_y: i32, max_y: i32) {
    for y in min_y..=max_y {
        for x in min_x..=max_x {
            print!("{}", if coords.contains(&(x, y)) { '#' } else { '.' })
        }
        println!()
    }
}

fn part1_and_2() {
    let mut min_max = i32::MAX;
    let mut coords = read_data().collect_vec();

    for i in 1.. {
        for ((x, y), (xs, ys)) in &mut coords {
            *x = *x + *xs;
            *y = *y + *ys;
        }

        let xs = coords.iter().map(|((x, _), _)| x);
        let ys = coords.iter().map(|((_, y), _)| y);

        match (xs.minmax(), ys.minmax()) {
            (MinMax(&lx, &gx), MinMax(&ly, &gy)) => {
                let diff = (gx - lx) + (gy - ly);

                if diff < min_max {
                    min_max = diff;
                } else {
                    println!("part 1:");

                    let result = coords
                        .into_iter()
                        .map(|((x, y), (xs, ys))| (x - xs, y - ys))
                        .collect::<HashSet<Position>>();
                    print_board(&result, lx, gx, ly, gy);

                    println!("part 2: {}", i - 1);
                    break;
                }

            }, _ => panic!("Wrong data")
        }
    }
}

fn main() {
    part1_and_2();
}