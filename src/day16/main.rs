#![feature(bindings_after_at, type_alias_impl_trait, generators, generator_trait)]

use std::cmp::*;
use std::io::BufRead;
use std::collections::{HashMap, HashSet};

use itertools::{Itertools, Chunk};
use once_cell::sync::Lazy;
use regex::Regex;

use OperationType::*;
use tools::gen_unsafe_iter;
use tools::utils::{get_buff_reader, get_file_contents};
use tools::generators::GeneratorIter;
use macros::{to_fixed_size_array, regex};

const FILE_PATH_PART_1: &'static str = "resources/day16/part1.txt";
const FILE_PATH_PART_2: &'static str = "resources/day16/part2.txt";

static REGISTRY_REGEX: Lazy<Regex> = regex!(r"\[(\d+), (\d+), (\d+), (\d+)\]");
static INSTRUCTION_REGEX: Lazy<Regex> = regex!(r"(\d+) (\d+) (\d+) (\d+)");
static CHUNK_REGEXPS: [&Lazy<Regex>; 3] = [&REGISTRY_REGEX, &INSTRUCTION_REGEX, &REGISTRY_REGEX];
const OPERATIONS: Lazy<HashMap<OperationType, fn(&mut Registry, &Operation)>> = Lazy::new(||{
    let mut hash_map = HashMap::<OperationType, fn(&mut Registry, &Operation)>::new();

    hash_map.insert(ADDR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) + r.g(b));
    hash_map.insert(ADDI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) + b);
    hash_map.insert(MULR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) * r.g(b));
    hash_map.insert(MULI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) * b);
    hash_map.insert(BANR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) & r.g(b));
    hash_map.insert(BANI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) & b);
    hash_map.insert(BORR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) | r.g(b));
    hash_map.insert(BORI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) | b);
    hash_map.insert(SETR, |r, &Operation{a, b: _b, c}| *r.s(c) = r.g(a));
    hash_map.insert(SETI, |r, &Operation{a, b: _b, c}| *r.s(c) = a);
    hash_map.insert(GTIR, |r, &Operation{a, b, c}| *r.s(c) = if a > r.g(b) { 1 } else { 0 });
    hash_map.insert(GTRI, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) > b { 1 } else { 0 });
    hash_map.insert(GTRR, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) > r.g(b) { 1 } else { 0 });
    hash_map.insert(EQIR, |r, &Operation{a, b, c}| *r.s(c) = if a == r.g(b) { 1 } else { 0 });
    hash_map.insert(EQRI, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) == b { 1 } else { 0 });
    hash_map.insert(EQRR, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) == r.g(b) { 1 } else { 0 });

    hash_map
});

#[derive(Debug, Hash, Eq, PartialEq, Ord, PartialOrd, Copy, Clone)]
enum OperationType {
    ADDR,
    ADDI,
    MULR,
    MULI,
    BANR,
    BANI,
    BORR,
    BORI,
    SETR,
    SETI,
    GTIR,
    GTRI,
    GTRR,
    EQIR,
    EQRI,
    EQRR
}

#[derive(Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Operation {
    a: usize,
    b: usize,
    c: usize,
}

type Registry = [usize; 4];
trait RegistryActions {
    fn s(&mut self, reg: usize) -> &mut usize;
    fn g(&self, reg: usize) -> usize;
}

impl RegistryActions for Registry {
    fn s(&mut self, reg: usize) -> &mut usize {
        &mut self[reg]
    }
    fn g(&self, reg: usize) -> usize {
        self[reg]
    }
}

type OpCode = usize;
type Input=(Registry, (OpCode, Operation), Registry);

fn parse_item(instructions: Chunk<impl Iterator<Item=String>>) -> [[usize; 4]; 3] {
    let parsed = gen_unsafe_iter! {
        for (instruction, regex) in instructions.zip(CHUNK_REGEXPS.iter()) {
            let item = gen_unsafe_iter! {
                for capture in regex.captures_iter(&instruction) {
                    for regex_match in capture.iter().skip(1).flatten() {
                        yield regex_match.as_str().parse::<usize>();
                    }
                }
            };

            let collected = item.collect::<Result<Vec<_>, _>>();
            let registries_states = collected.into_iter().flatten();

            yield to_fixed_size_array!(4, registries_states)
        }
    };

    to_fixed_size_array!(3, parsed)
}

fn read_codes() -> Vec<Input> {
    get_buff_reader(FILE_PATH_PART_1)
        .lines()
        .map(&Result::<_, _>::unwrap)
        .chunks(4)
        .into_iter()
        .map(&parse_item)
        .map(|[before, [op, a, b, c], after]| (before, (op, Operation { a, b, c }), after))
        .collect_vec()
}

fn get_matching_codes() -> impl Iterator<Item=(OpCode, HashSet<OperationType>)> {
    read_codes()
        .into_iter()
        .map(|(before, (op_code, operation), after)| {
            let matching = OPERATIONS
                .iter()
                .map(|(&op_type, op)| {
                    let mut cloned = before.clone();
                    op(&mut cloned, &operation);
                    (op_type, cloned)
                }).filter(|(_, modified)| *modified == after)
                  .map(|(op_type, _)| op_type)
                  .collect::<HashSet<_>>();

            (op_code, matching)
        })
}

fn part1() {
    let three_or_more_matching =
        get_matching_codes()
            .filter(|(_, m)| m.len() > 2)
            .count();

    println!("part 1: {}", three_or_more_matching);
}

fn sort_by_matching_size(results: &mut Vec<(usize, HashSet<OperationType>)>) {
    results.sort_by_key(|(_, m)|-1 * m.len() as i64);
}

fn find_opcodes() -> HashMap<usize, OperationType> {
    let mut intersections = HashMap::<OpCode, HashSet<OperationType>>::new();

    for (code, matching) in get_matching_codes() {
        let intersection =
            intersections
                .entry(code)
                .or_insert(matching.clone())
                .intersection(&matching)
                .map(|i| *i)
                .collect::<HashSet<_>>();

        intersections.insert(code, intersection);
    }

    let mut ordered = intersections.into_iter().collect_vec();
    let mut result = HashMap::<usize, OperationType>::with_capacity(ordered.len());

    sort_by_matching_size(&mut ordered);

    while let Some((op, matching_ops)) = ordered.pop() {
        for (_, next_matching_ops) in &mut ordered {
            let reduced = next_matching_ops.difference(&matching_ops);

            *next_matching_ops = reduced.map(|op| *op).collect::<HashSet<_>>();
        }

        result.insert(op, matching_ops.into_iter().next().unwrap());

        sort_by_matching_size(&mut ordered);
    };

    result
}

fn part2() {
    let opcodes = find_opcodes();
    let mut registry = [0usize; 4];

    for line in get_file_contents(FILE_PATH_PART_2) {
        let instruction = INSTRUCTION_REGEX
            .captures_iter(&line)
            .flat_map(|c| c.iter().flatten().flat_map(|m| m.as_str().parse::<usize>()).collect_vec())
            .collect_vec();

        if let [op, a, b, c] = instruction[..] {
            OPERATIONS[&opcodes[&op]](&mut registry, &Operation{a, b, c});
        }
    }

    println!("part 2: {}", registry[0]);
}

fn main() {
    part1();
    part2();
}