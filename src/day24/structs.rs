use std::collections::{BTreeSet, HashMap};

#[derive(PartialEq, Eq, PartialOrd, Ord, AsRefStr, EnumIter, Debug, Copy, Clone, Hash)]
pub enum AttackType {
    Radiation,
    Slashing,
    Bludgeoning,
    Fire,
    Cold
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord,  Copy, Clone, Hash)]
pub enum SquadType {
    Infection,
    ImmuneSystem
}

#[derive(Debug, Eq, PartialEq, Hash)]
pub struct Squad {
    pub id: UnitId,
    pub units: usize,
    pub unit_hp: usize,
    pub unit_damage: Damage,
    pub unit_damage_type: AttackType,
    pub squad_type: SquadType,
    pub initiative: Initiative,
    pub immunities: BTreeSet<AttackType>,
    pub weaknesses: BTreeSet<AttackType>
}

pub type UnitId = usize;
pub type Damage = usize;
pub type Initiative = usize;

#[derive(Debug, Eq, PartialEq)]
pub struct SquadPriority(pub UnitId, pub Damage, pub Initiative);

pub type AttackerId = UnitId;
pub type DefenderId = UnitId;

#[derive(Debug, Eq, PartialEq)]
pub struct Attack(pub Initiative, pub AttackerId, pub DefenderId);

pub type Squads = HashMap<UnitId, Squad>;

pub struct GameState {
    pub alive: Squads
}