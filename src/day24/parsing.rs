use once_cell::sync::Lazy;
use regex::Regex;
use std::collections::{HashMap, BTreeSet};

use strum::IntoEnumIterator;

use macros::{to_tuple, regex};
use tools::utils::get_file_contents;
use crate::structs::{*, SquadType::*};

const WEAK: &str = "weak";
const IMMUNE: &str = "immune";
const INFECTION_FILE_PATH: &str =  "resources/day24/infection.txt";
const IMMUNE_SYSTEM_FILE_PATH: &str = "resources/day24/immune_system.txt";

const UNITS_REGEX: Lazy<Regex> = regex!(
    r"(\d+) units each with (\d+) hit points (\(.+\) )?with an attack that does (\d+) (\w+) damage at initiative (\d+)");

const WEAKNESS_IMMUNITIES_REGEX: Lazy<Regex> =
    regex!(r"(((weak|immune) to ((\w+|, \w+)+))(; )?((immune|weak) to ((\w+|, \w+)+))?)");

const ATTACK_TYPE_MAPPING: Lazy<HashMap<String, AttackType>> = Lazy::new(||
    AttackType::iter()
        .map(|t| (t.as_ref().to_lowercase().to_string(), t))
        .collect::<HashMap<_, _>>()
);


fn parse_unit(line: String, squad_type: SquadType, id: UnitId) -> Option<Squad> {
    let captures = UNITS_REGEX.captures(&line)?;
    let (_, units, hit_points, weakness_immunities, damage, damage_type, initiative) =
        to_tuple!(7, captures.iter().map(|c| c.map(|g| g.as_str())));

    let mut results = HashMap::<&str, BTreeSet<AttackType>>::new();

    if let Some(capture) = weakness_immunities {
        let weaknesses_immunities_parsed = WEAKNESS_IMMUNITIES_REGEX.captures(capture)?;

        for (immune_or_weak, i) in vec![
            weaknesses_immunities_parsed.get(3).map(|t| (t, 4)),
            weaknesses_immunities_parsed.get(8).map(|t| (t, 9)),
        ].into_iter()
            .flatten() {
            let str = &weaknesses_immunities_parsed[i];
            let split = str
                .split(',')
                .map(|t| ATTACK_TYPE_MAPPING[t.trim()])
                .collect::<BTreeSet<_>>();

            results.insert(immune_or_weak.as_str(), split);
        }
    }

    Some(Squad {
        id,
        units: units.unwrap().parse::<usize>().unwrap(),
        unit_hp: hit_points.unwrap().parse::<usize>().unwrap(),
        unit_damage: damage.unwrap().parse::<usize>().unwrap(),
        unit_damage_type: ATTACK_TYPE_MAPPING[damage_type.unwrap()],
        initiative: initiative.unwrap().parse::<usize>().unwrap(),
        squad_type,
        weaknesses: results.entry(WEAK).or_default().clone(),
        immunities: results.entry(IMMUNE).or_default().clone()
    })
}

pub fn parse_units() -> HashMap<UnitId, Squad> {
    [(IMMUNE_SYSTEM_FILE_PATH, ImmuneSystem), (INFECTION_FILE_PATH, Infection)]
        .iter()
        .flat_map(|(p, t)| get_file_contents(p).map(move |l| (l, *t)))
        .enumerate()
        .flat_map(|(id, (l, t))| parse_unit(l, t, id))
        .map(|squad| (squad.id, squad))
        .collect()
}