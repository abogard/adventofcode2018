#![feature(bindings_after_at, map_first_last)]

mod parsing;
mod structs;
mod traits;

#[macro_use]
extern crate strum_macros;

use std::collections::BTreeSet;
use itertools::Itertools;
use std::cmp::max;
use if_chain::if_chain;

use structs::{*, SquadType::*, Squads};
use parsing::parse_units;

impl GameState {
    fn new(boost: impl Fn(&mut Squad)) -> GameState {
        let mut state = GameState { alive: parse_units() };
        state.apply_boost(boost);

        state
    }

    fn choose_target<'a>(attacker: &Squad, possible: impl Iterator<Item=&'a Squad>) -> Option<UnitId> {
        possible
            .filter(|defender| defender.squad_type != attacker.squad_type)
            .filter(|defender| !defender.immunities.contains(&attacker.unit_damage_type))
            .max_by_key(|defender| GameState::match_up(attacker, defender))
            .map(|squad| squad.id)
    }

    fn match_up(attacker: &Squad, defender: &Squad) -> (Damage, Damage, Initiative) {
        let has_weakness = defender.weaknesses.contains(&attacker.unit_damage_type);
        let damage_multiplier = if has_weakness { 2 } else { 1 };
        let attacker_damage = attacker.units * attacker.unit_damage * damage_multiplier;
        let defender_damage = defender.units * defender.unit_damage;

        (attacker_damage, defender_damage, defender.initiative)
    }

    fn apply_boost(&mut self, boost: impl Fn(&mut Squad)) {
        for (_, squad) in &mut self.alive { boost(squad); }
    }

    fn get_move_priorities(&self) -> BTreeSet<SquadPriority> {
        self.alive
            .iter()
            .map(|(&id, squad)| squad.move_priority(id))
            .collect()
    }

    fn should_play(&self) -> bool {
        self.alive
            .values()
            .map(|s| s.squad_type)
            .unique()
            .count() > 1
    }

    fn target_selection_phase(&mut self) -> BTreeSet<Attack> {
        let mut to_attack = BTreeSet::<Attack>::new();
        let mut to_move = self.get_move_priorities();
        let mut possible = self.alive.keys().cloned().collect::<BTreeSet<_>>();

        while let Some(SquadPriority(attacker_id, _, _)) = to_move.pop_first() {
            let attacker = &self[attacker_id];
            let targets = possible.iter().map(|&defender_id| &self[defender_id]);

            if let Some(defender) = GameState::choose_target(attacker, targets) {
                to_attack.insert(Attack(attacker.initiative, attacker.id, defender));

                possible.remove(&defender);
            }
        }

        to_attack
    }

    fn attack_phase(&mut self, mut to_attack: BTreeSet<Attack>) -> Damage {
        let mut dealt_damage = 0;

        while let Some(Attack(_, attacker, defender)) = to_attack.pop_first() {
            if !self.alive.contains_key(&attacker) || !self.alive.contains_key(&defender) {
                continue;
            }

            let unit_hp = self[defender].unit_hp;
            let defender_hp = self[defender].units * unit_hp;
            let (damage, _, _) = GameState::match_up(&self[attacker], &self[defender]);
            let killed_units = damage / unit_hp;

            dealt_damage = max(killed_units, dealt_damage);

            if defender_hp <= damage {
                self.alive.remove(&defender);
            } else {
                (&mut self[defender]).units -= killed_units;
            }
        }

        dealt_damage
    }

    fn fight(&mut self) -> Option<&Squads> {
        while self.should_play() {
            let to_attack = self.target_selection_phase();

            if to_attack.is_empty() {
                return None
            }

            if self.attack_phase(to_attack) == 0 {
                return None
            }
        }

        Some(&self.alive)
    }
}

fn part1() {
    let mut game = GameState::new(|_| {});
    let surviving_units = game
        .fight()
        .unwrap()
        .values()
        .map(|squad| squad.units)
        .sum::<usize>();

    println!("part 1: {}", surviving_units)
}

fn part2() {
    for i in 1usize.. {
        let boost = |s: &mut Squad| if s.squad_type == ImmuneSystem { s.unit_damage += i };
        let mut state = GameState::new(boost);

        if_chain! (
            if let Some(result) = state.fight();
            if let Some(squad) = result.values().next();
            if squad.squad_type == ImmuneSystem;

        then {
            let surviving_units = result
                .values()
                .map(|squad| squad.units)
                .sum::<usize>();

            println!("part 2: {:?}", surviving_units);

            return;
        });
    }

    println!("part 2: A solution hasn't been found");
}

fn main() {
    part1();
    part2();
}