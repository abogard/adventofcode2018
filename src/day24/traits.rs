use std::cmp::Ordering;

use crate::structs::*;
use std::ops::{IndexMut, Index};

impl PartialOrd for SquadPriority {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let order = (self.1, self.2).partial_cmp(&(other.1, other.2));
        order.map(Ordering::reverse)
    }
}

impl Ord for SquadPriority {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl Squad {
    pub fn move_priority(&self, id: UnitId) -> SquadPriority {
        SquadPriority(id, self.units * self.unit_damage, self.initiative)
    }
}

impl PartialOrd for Attack {
    fn partial_cmp(&self, Attack(initiative, _, _): &Self) -> Option<Ordering> {
        self.0.partial_cmp(&initiative).map(Ordering::reverse)
    }
}

impl Ord for Attack {
    fn cmp(&self, other: &Self) -> Ordering { self.partial_cmp(other).unwrap() }
}

impl Index<UnitId> for GameState {
    type Output = Squad;

    fn index(&self, index: UnitId) -> &Self::Output {
        &self.alive[&index]
    }
}

impl IndexMut<UnitId> for GameState {
    fn index_mut(&mut self, index: UnitId) -> &mut Self::Output {
        self.alive.get_mut(&index).unwrap()
    }
}