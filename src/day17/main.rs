use std::fs::File;
use regex::Regex;
use std::ops::{Range, Index, IndexMut};
use std::io::Write;
use std::collections::BinaryHeap;
use itertools::{Itertools, MinMaxResult::MinMax};
use once_cell::sync::Lazy;

use macros::regex;
use tools::{utils::get_file_contents, eq_or};

const FILE_PATH: &str = "resources/day17/part11.txt";
const COORDINATES_REGEX: Lazy<Regex> = regex!(r"(.)=(\d+), (.)=(\d+)\.\.(\d+)");

#[allow(dead_code)]
struct Board {
    max_x: usize,
    max_y: usize,
    pre_normalized_min_x: usize,
    pre_normalized_min_y: usize,
    board_data: Vec<Vec<char>>
}

fn read_coordinates() -> impl Iterator<Item=(Range<usize>, Range<usize>)> {
    get_file_contents(FILE_PATH)
        .map(move |str| {
            let result = COORDINATES_REGEX.captures(str.as_str()).unwrap();

            let coord1 = result[2].parse::<usize>().unwrap();
            let coord2 = result[4].parse::<usize>().unwrap();
            let coord3 = result[5].parse::<usize>().unwrap();
            let l_range = coord1..coord1 + 1;
            let r_range = coord2..coord3 + 1;

            if result[1].eq("x") {
                (l_range, r_range)
            } else {
                (r_range, l_range)
            }
        })
}

fn get_board() -> Board {
    let minmax_x = read_coordinates().flat_map(|(xs, _)| xs).minmax();
    let minmax_y = read_coordinates().flat_map(|(_, ys)| ys).minmax();

    match (minmax_x, minmax_y) {
        (MinMax(min_x, max_x), MinMax(min_y, max_y)) => {
            let normalized_max_x = max_x - min_x + 3;
            let normalized_max_y = max_y - min_y + 2;

            let mut board_data = vec![vec!['.'; normalized_max_x]; normalized_max_y];

            for (range_x, range_y) in read_coordinates() {
                for y in range_y {
                    for x in range_x.clone() {
                        board_data[y - min_y + 1][x - min_x + 1] = '#';
                    }
                }
            }

            Board {
                max_x: normalized_max_x,
                max_y: normalized_max_y,
                pre_normalized_min_x: min_x,
                pre_normalized_min_y: min_y,
                board_data
            }
        },
        _ => panic!("Incorrect board data has been provided!")
    }
}
impl Index<usize> for Board {
    type Output=Vec<char>;

    fn index(&self, index: usize) -> &Self::Output {
        &self.board_data[index]
    }
}

impl IndexMut<usize> for Board {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.board_data[index]
    }
}

impl Board {
    fn find_limiting_wall(&self, range: impl Iterator<Item=usize>, y: usize, x: usize) -> usize {
        range
            .take_while(|&sx| self[y][sx] != '#' && self[y + 1][sx] != '.')
            .last()
            .unwrap_or(x)
    }

    #[allow(dead_code)]
    fn store_board(&self, file_name: &str) {
        let mut file = File::create(file_name).unwrap();

        for row in &self.board_data {
            for &col in row {
                file.write(vec![col as u8].as_slice()).unwrap();
            }
            file.write(vec!['\n' as u8].as_slice()).unwrap();
        }
    }

    fn get_new_source(&self, x: usize, y: usize, offset: isize) -> Option<(usize, usize)> {
        let offset_x = (x as isize + offset) as usize;
        let offset_y = y + 1;

        if self[offset_y][x] == '|' {
            Some((x, offset_y))
        } else if eq_or!{ self[y][offset_x], '.', '|' } && self[offset_y][offset_x] == '.' {
            Some((offset_x, offset_y))
        } else {
            None
        }
    }

    fn iter(&self) -> impl Iterator<Item=&Vec<char>> {
        self.board_data.iter()
    }
}

fn main() {
    let mut board = get_board();
    let min_x = board.pre_normalized_min_x;
    let max_x = board.max_x;
    let max_y = board.max_y;

    let mut sources: BinaryHeap<(usize, usize)> = BinaryHeap::new();

    sources.push((500 - min_x, 0));

    while let Some((x, mut y)) = sources.pop() {
        if  board[y][x] == '~' { continue; }

        while y < max_y && (board[y][x] == '.' || board[y][x] == '|') {
            board[y][x] = '|';
            y += 1;
        }

        if y > max_y - 1 || y < 1 || board[y][x] == '|' { continue; }

        y -= 1;

        loop {
            let right = board.find_limiting_wall(x..max_x, y, x);
            let left = board.find_limiting_wall((0..x).rev(), y, x);
            let right_source = board.get_new_source(right, y, 1);
            let left_source = board.get_new_source(left, y, -1);
            let has_new_source = right_source.is_some() || left_source.is_some();

            for source in vec![right_source, left_source].into_iter().flatten() {
                sources.push(source);
            }

            for nx in left..right + 1 {
                board[y][nx] = if has_new_source { '|' } else { '~' };
            }

            if has_new_source { break; }
            else { y -= 1; }
        }
    }
    let water =
        board
            .iter()
            .flat_map(|col| col.iter())
            .filter(|&&char| char == '|' || char == '~')
            .count();

    let still =
        board
            .iter()
            .flat_map(|col| col.iter())
            .filter(|&&char| char == '~')
            .count();

    println!("part 1: {:?}", water - 1);
    println!("part 2: {:?}", still);
}