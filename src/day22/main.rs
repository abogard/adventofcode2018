#![feature(box_syntax, bindings_after_at)]

use regex::Regex;
use once_cell::sync::Lazy;

use std::error::Error;
use std::cmp::{min, Ordering};
use std::collections::{BinaryHeap, HashSet, HashMap};

use Equipment::*;
use macros::regex;
use tools::utils::get_file_contents;

const EROSION_MOD: isize = 20183;
const FILE_PATH: &str = "resources/day22/part1.txt";
const DEPTH_REGEX: Lazy<Regex> = regex!(r"depth: (\d+)");
const TARGET_REGEX: Lazy<Regex> = regex!(r"target: (\d+),(\d+)");

type Depth = usize;
type Point = (usize, usize);
type Target = Point;
type Matrix = Vec<Vec<isize>>;

#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, Debug)]
enum Equipment {
    Torch,
    ClimbingGear,
    Neither
}

type Node = (Point, Equipment);

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
struct Region {
    node: Node,
    distance: usize,
}

#[derive(PartialEq, Eq, Hash, Debug)]
struct Board {
    depth: usize,
    target: Point,
    points: Matrix
}

impl PartialOrd for Region {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.distance.partial_cmp(&other.distance).map(|o| o.reverse())
    }
}

impl Ord for Region {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(&other).unwrap()
    }
}

fn read_data() -> Result<(Depth, Target), Box<dyn Error>> {
    let mut lines = get_file_contents(FILE_PATH);

    let first = lines.next().ok_or("File is to short!")?;
    let depth_capture = DEPTH_REGEX
        .captures(&first)
        .filter(|c| c.len() > 1)
        .ok_or("Depth data is malformed!")?;

    let second = lines.next().ok_or("File is to short!")?;
    let target_capture = TARGET_REGEX
        .captures(&second)
        .filter(|c| c.len() > 2)
        .ok_or("Target data is malformed!")?;

    let depth = depth_capture[1].parse::<usize>()?;
    let target = (target_capture[1].parse::<usize>()?, target_capture[2].parse::<usize>()?);

    Ok((depth, target))
}

fn generate_index(board: &mut Board, (x, y): Point) -> isize {
    let rows =  board.points.len();
    let columns =  board.points[rows - 1].len();

    for _ in rows..=y {
        board.points.push(vec![-1; columns]);
    }

    for _ in columns..=x {
        for ys in 0.. board.points.len() {
            board.points[ys].push(-1);
        }
    }

    if  board.points[y][x] != -1 { board.points[y][x] }
    else {
        let erosion_level = match (x, y) {
            (0, 0) => 0,
            region if region == board.target => 0,
            (xs, 0) => (16807 * xs as isize),
            (0, ys) => (48271 * ys as isize),
            (xs, ys) =>
                generate_index(board, (xs - 1, y)) *
                generate_index(board, (xs, ys - 1))
        };

        board.points[y][x] = (erosion_level + board.depth as isize) % EROSION_MOD;
        board.points[y][x]
    }
}

fn create_board() -> Result<Board, Box<dyn Error>> {
    let (depth, target@(x, y)) = read_data()?;
    let mut board = Board{ depth, target, points: vec![vec![-1isize]] };

    for ys in 0..=y {
        for xs in 0..=x {
            generate_index(&mut board, (xs, ys));
        }
    }

    Ok(board)
}

fn part1() {
    let board = create_board().unwrap();

    let risk_level = board
        .points
        .iter()
        .map(|c| c.iter().map(|&i| i % 3).sum::<isize>())
        .sum::<isize>();

    println!("part 1: {}", risk_level);
}

fn get_adjacent_points(Region { node: ((x, y), _), distance: _distance}: Region) -> Vec<Point> {
    let mut adjacent = vec![(x + 1, y), (x, y + 1)];

    if x > 0 { adjacent.push((x - 1, y)) }
    if y > 0 { adjacent.push((x, y - 1)) }

    adjacent
}

fn get_different_region(
    Region { node: (_, equipment), distance }: Region,
    neighbour: Point,
    geological_index: isize
) -> Region {
    let new_equipment = match (equipment, geological_index) {
        (Torch, 0) => ClimbingGear,
        (Torch, 2) => Neither,
        (Neither, 1) => ClimbingGear,
        (Neither, 2) => Torch,
        (ClimbingGear, 0) => Torch,
        (ClimbingGear, 1) => Neither,
        _ => panic!("Incorrect data!!")
    };

    Region { node: (neighbour, new_equipment), distance: distance + 8}
}

fn get_adjacent_regions(
    region@Region { node: (current, equipment), distance }: Region,
    neighbour: Point,
    mut board: &mut Board,
) -> Vec<Region> {
    let current_index = generate_index(&mut board, current) % 3;
    let neighbour_index = generate_index(&mut board, neighbour) % 3;

    let same_region = Region{ node: (neighbour, equipment), distance: distance + 1};
    let different_region = get_different_region(region, neighbour, current_index);

    match (equipment, current_index, neighbour_index) {
        (Torch, 0, 2) |
        (Torch, 2, 0) |
        (Neither, 2, 1) |
        (Neither, 1, 2) |
        (ClimbingGear, 1, 0) |
        (ClimbingGear, 0, 1) => vec![same_region],
        (_, c_i, n_i) if c_i == n_i => vec![same_region, different_region],
        _ => vec![different_region],
    }
}

fn abs_diff(l: usize, r: usize) -> usize {
    if l > r { l - r } else { r - l }
}

fn part2() {
    let mut board = create_board().unwrap();
    let (tx, ty) = board.target;
    let mut queue = BinaryHeap::<Region>::new();
    let mut visited = HashSet::<Node>::new();
    let mut distances = HashMap::<Node, usize>::new();
    let mut shortest = std::usize::MAX;

    let start = Region { node: ((0, 0), Torch), distance: 0 };

    queue.push(start);
    distances.insert(start.node, start.distance);

    while let Some(region@ Region { node, distance: _distance }) = queue.pop() {
        if !visited.insert(node) {
            continue;
        }

        for adjacent_point@(ax, ay) in get_adjacent_points(region) {
            if abs_diff(ax, tx) + abs_diff(ay,ty) > shortest / 2 {
                continue;
            }

            for adjacent_region in get_adjacent_regions(region, adjacent_point ,&mut board) {
                let Region {node: adjacent_node@(_, equipment), mut distance} = adjacent_region;

                if adjacent_point == board.target {
                    distance += if equipment != Torch {7} else {0};
                    shortest = min(shortest, distance);
                }

                if let Some(&old_distance) = distances.get(&adjacent_node) {
                    if distance < old_distance { distances.insert(adjacent_node, distance); }
                } else {
                    distances.insert(adjacent_node, distance);
                }

                queue.push(adjacent_region);
            }
        }
    }

    println!("part 2: {:?}", shortest);
}

#[allow(dead_code)]
fn draw_board(Board{depth: _, target: _, points}: &Board) {
    for y in 0..points.len() {
        for x in 0..points[y].len() {
            print!("{}", match points[y][x] % 3 {
                0 => '.',
                1 => '=',
                2 => '|',
                _ => '?'
            });
        }
        println!();
    }
}

fn main() {
    part1();
    part2();
}