#![feature(box_syntax, bindings_after_at)]

use peg;
use Direction::*;
use Directions::*;
use std::collections::{HashMap, VecDeque};
use once_cell::sync::Lazy;
use std::fs::read_to_string;
use itertools::{Itertools, MinMaxResult::MinMax};
use tools::eq_or;

type Point = (i64, i64);
type Board = HashMap<Point, char>;

#[derive(Debug, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub enum Direction {
    W,
    E,
    N,
    S,
    EMPTY
}

const FILE_PATH: &'static str = "resources/day20/part1.txt";

static DIRECTIONS_MAPPING: Lazy<HashMap<Direction, Point>> = Lazy::new(||{
    let mut map = HashMap::<Direction, Point>::new();

    map.insert(W, (-1,  0));
    map.insert(E, ( 1,  0));
    map.insert(N, ( 0, -1));
    map.insert(S, ( 0,  1));

    map
});

#[derive(Debug)]
pub enum Directions {
    Dir(Direction),
    Sequence(Box<Vec<Directions>>),
    Alternative(Box<Vec<Directions>>)
}

type Regexps = Vec<Directions>;

peg::parser!{
    grammar dir_regex() for str {
        rule direction() -> Directions = d:$(['W' | 'E' | 'N' | 'S']) {
            Dir(match d {
                "W" => W,
                "E" => E,
                "N" => N,
                "S" => S,
                ""  => EMPTY,
                 c  => panic!(format!("{} isn't a valid direction!", c))
            })
        }
        rule alt() -> Directions = "(" a:(alt() / seq()) ** "|" ")" { Alternative(box a) }
        rule seq() -> Directions = s:(alt() / direction()) * { Sequence(box s) }
        rule reg() -> Directions = "^" r:(alt() / seq()) "$" { r }
        pub rule regs() -> Regexps = rs:(reg() +) { rs }
    }
}

fn process_directions(board: &mut Board, pos: Point, directions: &Directions) -> Point {
    match directions {
        Dir(d) => {
            let door = move_to(pos, d);
            board.insert(door, if eq_or!(*d, E, W) { '|' } else { '-' });

            let new_position = move_to(door, d);
            board.insert(new_position, '.');

            new_position
        }
        Alternative(alts) => {
            for alt in alts.iter() {
                process_directions(board, pos, alt);
            }
            pos
        }
        Sequence(seq) =>
            seq
                .iter()
                .fold(pos, |loc, reg| process_directions(board, loc, reg))
    }
}

#[allow(dead_code)]
fn draw_board(board: &Board) {
    match (board.keys().minmax_by_key(|&(x, _)| x), board.keys().minmax_by_key(|&(_, y)| y)) {
        (MinMax(&(min_x, _), &(max_x, _)), MinMax(&(_, min_y), &(_, max_y))) => {
            for y in min_y-1..=max_y+1 {
                for x in min_x-1..=max_x+1 {
                    print!("{}", if let Some(c) = board.get(&(x, y)) { *c } else { '#' });
                }
                println!();
            }
        },
        _ => panic!("Wrong data!!")
    }
}

fn move_to((x, y): Point, dir: &Direction) -> Point {
    let (xs, ys) = DIRECTIONS_MAPPING[dir];

    (x + xs, y + ys)
}

fn find_most_doors(board: &Board, start: Point) -> HashMap<Point, usize> {
    let mut queue = VecDeque::<(usize, Point)>::new();
    let mut distances = HashMap::<Point, usize>::new();

    queue.push_back((0, start));

    while let Some((d, pos)) = queue.pop_front() {
        if distances.contains_key(&pos) {
            continue;
        }

        distances.insert(pos, d);

        for (dir, new_pos) in DIRECTIONS_MAPPING
            .keys()
            .map(|dir| (dir, move_to(pos, dir)))
            .filter(|(_, new_pos)| board.get(new_pos).iter().any(|&&c| eq_or! (c, '-', '|'))) {
            queue.push_back(((d+1), move_to(new_pos, dir)))
        }

    }

    distances
}

fn read_board() -> Board {
    let mut board = Board::new();
    board.insert((0, 0), 'X');
    let input = read_to_string(&FILE_PATH).unwrap();

    for reg in &dir_regex::regs(&input).unwrap() {
        process_directions(&mut board, (0, 0), reg);
    }

    board
}

fn part1() {
    let distances = find_most_doors(&read_board(), (0, 0));

    println!("part 1: {:?}", distances.values().max());
}

fn part2() {
    let distances = find_most_doors(&read_board(), (0, 0));

    println!("part 2: {:?}", distances.values().filter(|&&d| d >= 1000).count());
}

fn main() {
    part1();
    part2();
}