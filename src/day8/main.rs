use std::fs::read_to_string;
use itertools::Itertools;

const FILE_PATH: &str = "resources/day8/part1.txt";

#[derive(Debug)]
struct Tree {
    metas: Vec<usize>,
    children: Vec<Tree>
}

fn read_child(mut iter: &mut dyn Iterator<Item=usize>) -> Tree {
    if let (Some(children_count), Some(meta_size)) = (iter.next(), iter.next()) {
        let children = (0..children_count)
            .scan(&mut iter, |acc, _| Some(read_child(acc)))
            .collect::<Vec<Tree>>();

        let metas = iter.take(meta_size).collect_vec();

        Tree { metas, children }
    } else {
        panic!("Data has an incorrect format!")
    }
}

fn read_tree() -> std::io::Result<Tree> {
    let string = read_to_string(FILE_PATH)?;
    let mut iter =
        string
            .split_whitespace()
            .map(str::parse::<usize>)
            .flatten();

    Ok(read_child(&mut iter))
}

fn part1(tree: &Tree) -> usize {
    tree.metas.iter().sum::<usize>() + tree.children.iter().map(part1).sum::<usize>()
}

fn part2(tree: &Tree) -> usize {
    match tree.children.len() {
        0 => tree.metas.iter().sum::<usize>(),
        _ =>
            tree
                .metas
                .iter()
                .map(|&idx| idx - 1)
                .filter(|&idx| idx < tree.children.len())
                .map(|idx| part2(&tree.children[idx]))
                .sum::<usize>()
    }
}

fn main() {
    let tree = read_tree().unwrap();

    println!("part 1: {:?}", part1(&tree));
    println!("part 2: {:?}", part2(&tree));
}