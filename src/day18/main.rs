use itertools::Itertools;
use std::cmp::{min, max};
use std::collections::HashMap;

use crate::Result::{OccurrencesSum, OccurrencesLoop};
use tools::utils::get_file_contents;

const FILE_PATH: &'static str = "resources/day18/part1.txt";

type Board = Vec<Vec<char>>;
type Occurrence = usize;
type RepeatIndex = usize;

#[derive(Debug)]
enum Result {
    OccurrencesSum(Occurrence),
    OccurrencesLoop(RepeatIndex, Vec<Occurrence>)
}

fn read_board() -> Board {
    get_file_contents(FILE_PATH)
        .map(|line| line.chars().collect_vec())
        .collect_vec()
}

fn count_occurrences(board: &Board) -> Occurrence {
    board
        .iter()
        .flat_map(|row| row)
        .sorted()
        .group_by(|&&c| c)
        .into_iter()
        .filter(|&(c, _)| c != '.')
        .map(|(_, items)| items.count())
        .fold(1, |acc, item| acc * item)
}

fn find_pattern(repetitions: usize) -> Option<Result> {
    let mut board = read_board();
    let mut occurrences = HashMap::<Board, usize>::new();

    for i in 0..repetitions {

        if let Some(&round) = occurrences.get(&board) {
            return Some(OccurrencesLoop(i, occurrences
                .iter()
                .filter(|(_, &r)| r >= round)
                .sorted_by_key(|(_, &r)| r)
                .map(|(b, _)| count_occurrences(b))
                .collect_vec()
            ));
        }

        let mut new_board = board.clone();

        for yn in 0usize..board.len() {
            let top_neighbour_y = max(0 as i64, yn as i64 - 1) as usize;
            let bottom_neighbour_y = min(yn + 1, board.len() - 1);

            for xn in 0usize..board[yn].len() {
                let left_neighbour_x = max(0, xn as i64 - 1) as usize;
                let right_neighbour_x = min(xn + 1, board[yn].len() - 1);

                let mut trees = 0usize;
                let mut lumberyard = 0usize;

                for y in top_neighbour_y..=bottom_neighbour_y {
                    for x in left_neighbour_x..=right_neighbour_x {
                        match board[y][x] {
                            _ if y == yn && x == xn => continue,
                            '|' => trees += 1,
                            '#' => lumberyard += 1,
                            _ => ()
                        };
                    }
                }

                match board[yn][xn] {
                    '.' if trees > 2 => new_board[yn][xn] = '|',
                    '|' if lumberyard > 2 => new_board[yn][xn] = '#',
                    '#' if lumberyard > 0 && trees > 0 => new_board[yn][xn] = '#',
                    '#' => new_board[yn][xn] = '.',
                    e => new_board[yn][xn] = e
                }
            }
        }

        if i + 1 == repetitions { return Some(OccurrencesSum(count_occurrences(&new_board))) }
        else { occurrences.insert(board, i); }

        board = new_board;
    }

    None
}

fn calculate_solution(repetitions: usize) -> Option<usize> {
    let results = find_pattern(repetitions)?;

    Some(match results {
        OccurrencesLoop(repeat, result) => {
            let reduced = repetitions - repeat;
            let looping = reduced % result.len();

            result[looping]
        },
        OccurrencesSum(sum) => sum
    })
}

fn main() {
    println!("part 1: {:?}", calculate_solution(10));
    println!("part 2: {:?}", calculate_solution(1000000000));
}