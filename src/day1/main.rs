use std::fs;
use std::collections::HashSet;

static FILE_PATH: &str = "resources/day1/part1.txt";

fn convert_to_int(text: &str) -> i64 {
    let error = format!("String {:?} cant be converted to int", text);

    text.parse::<i64>().expect(error.as_str())
}

fn get_file_contents() -> String {
    let error = format!("File {:?} doesn't exist", FILE_PATH);

    fs::read_to_string(FILE_PATH).expect(error.as_str())
}

fn part1() {
    let sum =
        get_file_contents()
            .lines()
            .map(convert_to_int)
            .sum::<i64>();

    println!("Part 1: {:?}", sum)
}

fn part2() {
    let first_repeated: i64 =
        get_file_contents()
            .lines()
            .map(convert_to_int)
            .cycle()
            .scan((false, HashSet::<i64>::new(), 0),  |(_, set, sum), item| {
                *sum = *sum + item;

                Some((!set.insert(*sum), *sum))
            }).filter_map(|(repeated, item)| if repeated { Some(item) } else { None })
            .next()
            .expect("A repeated element hasn't been found!");

    println!("Part 2: {:?}", first_repeated)
}

fn main() {
    part1();
    part2();
}