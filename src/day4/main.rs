use regex::Regex;
use chrono::prelude::*;
use chrono::Duration;
use std::collections::HashMap;
use itertools::Itertools;
use once_cell::sync::Lazy;

use macros::regex;
use tools::utils::get_file_contents;

const FILE_PATH: &str = "resources/day4/part1.txt";
const LOG_REGEX: Lazy<Regex> = regex!(r"\[(.+)\] (.+)");
const SHIFT_REGEX: Lazy<Regex> = regex!(r"Guard [#](\d+) begins shift");

type Minutes = usize;
type GuardId = i64;
type WhenAsleepMap = HashMap<GuardId, Vec<Minutes>>;
type GuardShift = String;
type DiaryLog = (DateTime<Utc>, GuardShift);
type Diary = Vec<DiaryLog>;

fn read_diary() -> Vec<(DateTime<Utc>, String)> {
    let mut diary = Diary::new();

    for line in get_file_contents(FILE_PATH) {
        for regex_match in LOG_REGEX.captures(line.as_str()) {
            let shift_log = regex_match[2].to_string();
            let shift_time = Utc
                .datetime_from_str(&regex_match[1], "%Y-%m-%d %H:%M")
                .expect(&format!("Couldn't parse {} as date!", &regex_match[1]));

            diary.push((shift_time, shift_log));
        }
    }

    diary.sort_by(|(l, _), (r, _)| l.signed_duration_since(*r).cmp(&Duration::zero()));
    diary
}

fn calculate_minutes_per_guard() -> WhenAsleepMap {
    let grouped = read_diary()
        .iter()
        .scan(-1 as GuardId, |id, (date, entry)| {
            if let Some(capture) = SHIFT_REGEX.captures(entry) {
                *id = (&capture[1]).parse::<GuardId>().unwrap();

                Some(None)
            } else {
                Some(Some((*id, date.minute() as usize)))
            }
        }).flatten()
          .into_group_map();

    let mut asleep = WhenAsleepMap::new();

    for (key, values) in grouped {
        for pair in values.chunks(2) {
            if let [begin, end] = pair {
                let entry = asleep
                    .entry(key)
                    .or_insert(vec![0;60]);

                for minute in *begin..*end {
                    entry[minute] = entry[minute] + 1
                }
            }
        }
    }

    asleep
}

fn part1() {
    let asleep = calculate_minutes_per_guard();

    let (&id, minutes) = asleep
        .iter()
        .max_by_key(|(_, v)| v.iter().sum::<usize>())
        .unwrap();

    let (max_minute, _) =
        minutes
            .iter()
            .enumerate()
            .max_by_key(|(_, &minute)| minute)
            .unwrap();

    println!("part 1: {:?}*{:?}={:?}", id, max_minute, id * max_minute as i64)
}

fn part2() {
    let asleep = calculate_minutes_per_guard();
    let (&id, (max_minute, _)) = asleep
        .iter()
        .flat_map(|(id, minutes)| minutes.iter().enumerate().map(move |e| (id, e) ))
        .max_by_key(|(_,(_, &minute))| minute)
        .unwrap();

    println!("part 2: {:?}*{:?}={:?}", id, max_minute, id * max_minute as i64)
}

fn main() {
    part1();
    part2();
}