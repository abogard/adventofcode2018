use regex::Regex;
use if_chain::if_chain;

use tools::utils;
use macros::regex;
use once_cell::sync::Lazy;

const FILE_PATH: &str = "resources/day3/part1.txt";
const CLAIM_REGEX: Lazy<Regex> = regex!(r"[#](\d+) [@] (\d+)[,](\d+)[:] (\d+)x(\d+)");

type Id = usize;
type SidesSizes = (usize, usize);
type RectangleVertices = (usize, usize);

type Claims = Vec<(Id, RectangleVertices, SidesSizes)>;

fn gather_claims() -> Claims {

    let mut claims = Claims::new();

    for ref line in utils::get_file_contents(FILE_PATH) {
        for ref regex_match in CLAIM_REGEX.captures(line.as_str()) {
            if_chain!(
                if let Ok(parsed) = regex_match
                    .iter()
                    .skip(1) // skip full string match
                    .flatten()
                    .map(|x| x.as_str().parse::<usize>())
                    .collect::<Result<Vec<_>, _>>();
                if let [id, x, y, dx, dy] = parsed[..];
            then {
                claims.push((id, (x, y), (dx, dy)));
            } else {
                panic!(format!("Failed to parse line {}!", line))
            })
        }
    }

    claims
}

fn part1() {
    let claims = gather_claims();
    let (xs, ys): (Vec<_>, Vec<_>) =
        claims
            .iter()
            .map(|(_, (x, y), (dx, dy))| (*x + *dx, *y + *dy))
            .unzip();

    let max_x = xs.into_iter().max().unwrap();
    let max_y = ys.into_iter().max().unwrap();
    let mut fabric = vec![0; max_x * max_y];

    for (_, (x, y), (xd, yd)) in claims {
        for xc  in x .. (x + xd) {
            for yc in y .. (y + yd) {
                let index = xc + yc * max_x;
                fabric[index] = fabric[index] + 1;
            }
        }
    }

    let intersecting =
        fabric
            .into_iter()
            .filter(|x| *x > 1)
            .count();

    println!("part 1: {:?}", intersecting);
}

fn intersects(
    ((x1, y1),(dx1, dy1)): &(&RectangleVertices, &SidesSizes),
    ((x2, y2),(dx2, dy2)): &(&RectangleVertices, &SidesSizes)
) -> bool {
    (x1 == x2 || x1 > x2 && x2 + dx2 > *x1 || x1 < x2 && x1 + dx1 > *x2)
 && (y1 == y2 || y1 > y2 && y2 + dy2 > *y1 || y1 < y2 && y1 + dy1 > *y2)
}

fn find_non_intersecting() -> Option<usize> {
    let claims = gather_claims();

    'outer: for (id1, start1, dist1) in &claims {
        for (id2, start2, dist2) in &claims  {

            if id1 == id2 {
                continue;
            } else if intersects(&(start1, dist1), &(start2, dist2)) {
                continue 'outer
            }
        }

        return Some(*id1)
    }

    None
}

fn part2() {
    println!("part 2: {:?}", find_non_intersecting().unwrap());
}

fn main() {
    part1();
    part2();
}