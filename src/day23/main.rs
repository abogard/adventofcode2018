#![feature(bindings_after_at, box_syntax, fn_traits)]

use once_cell::sync::Lazy;
use regex::Regex;

use tools::utils::{get_file_contents, combine};
use tools::graph::{Graph as UndirectedGraph};
use macros::{to_fixed_size_array, to_tuple};
use itertools::{Itertools, MinMaxResult::MinMax};
use std::collections::{HashMap, HashSet, BinaryHeap};
use nalgebra::Vector3;
use num_traits::{AsPrimitive, Zero};
use std::cmp::Ordering;
use std::fmt::Debug;

use macros::regex;
use tools::geometry::geometry3d::*;

const FILE_PATH: &str = "resources/day23/part1.txt";
static ZERO: Lazy<Vector3<i64>> = Lazy::new(|| Vector3::zero());
static LINE_REGEX: Lazy<Regex> = regex!(r"pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)");

fn read_data() -> impl Iterator<Item=(Vector3<i64>, i64)> {
    get_file_contents(FILE_PATH)
        .flat_map(|line|
            LINE_REGEX
                .captures(&line)
                .map( |c| to_fixed_size_array!(4, c
                    .iter()
                    .skip(1)
                    .flatten()
                    .flat_map(|s| s.as_str().parse::<i64>()))
                ).map(|[x, y, z, r]| (Vector3::new(x, y, z), r))
        )
}

fn part1()  {
    let nanobots = read_data().collect_vec();
    let (m, r) =
        nanobots
            .iter()
            .max_by_key(|(_, r)| *r)
            .unwrap();

    let in_range = nanobots
        .iter()
        .filter(|&(n, _)| manhattan_distance(m, n) <= *r)
        .count();

    println!("part 1: {:?}", in_range);
}

type Nanobot = (IPoint, Radius);
type Clique = HashMap<IPoint, Radius>;
type Neighbours = HashSet<Nanobot>;

#[repr(transparent)]
struct Graph(HashMap<Nanobot, Neighbours>);

#[repr(transparent)]
#[derive(PartialEq, Eq, Debug)]
struct CuboidWrapper(Cuboid);

impl UndirectedGraph<Nanobot> for Graph {
    fn get_nodes<'a>(&'a self) -> Box<dyn Iterator<Item=&'a Nanobot> + 'a> {
        box self.0.keys()
    }

    fn get_neighbours<'a>(&'a self, node: &Nanobot) -> Box<dyn Iterator<Item=&'a Nanobot> + 'a> {
        box self.0[node].iter()
    }
}

impl PartialOrd for CuboidWrapper {
    fn partial_cmp(&self, CuboidWrapper((other_corner, _)): &Self) -> Option<Ordering> {
        let CuboidWrapper((self_corner, _)) = self;

        let left = manhattan_distance(self_corner, &ZERO);
        let right = manhattan_distance(other_corner, &ZERO);

        Some(left.cmp(&right).reverse())
    }
}

impl Ord for CuboidWrapper {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

fn is_in_range<T: 'static + Debug + PartialEq, S: 'static + Debug + PartialEq>(
    (p1, r1): &(Point<T>, T),
    (p2, r2): &(Point<S>, S)
) -> bool
    where T: CommonNumericType<S> + AsPrimitive<<T as CommonNumericType<S>>::CommonType>,
          S: AsPrimitive<<T as CommonNumericType<S>>::CommonType> {
    manhattan_distance(p1, p2) <= r1.as_() + r2.as_()
}

fn get_largest_clique() -> Clique {
    let nanobots = read_data().collect_vec();
    let mut graph = HashMap::<Nanobot, Neighbours>::new();

    for i in 0..nanobots.len() {
        let current = &nanobots[i];
        for j in i+1..nanobots.len() {
            let next = &nanobots[j];

            if is_in_range(current, next) {
                graph
                    .entry(current.clone())
                    .or_insert(Neighbours::new())
                    .insert(next.clone());

                graph
                    .entry(next.clone())
                    .or_insert(Neighbours::new())
                    .insert(current.clone());
            }
        }
    }

    Graph(graph)
        .maximum_clique()
        .into_iter()
        .cloned()
        .collect::<Clique>()
}

fn get_nanobot_grouped_corners(
    &(position, radius): &Nanobot
) -> impl Iterator<Item=impl Iterator<Item=IPoint>> {
    (0..3).map(move |i| {
        let mut min_corner = position.clone_owned();
        let mut max_corner = position.clone_owned();
        min_corner[i] -= radius;
        max_corner[i] += radius;

        vec![min_corner, max_corner].into_iter()
    })
}

fn get_nanobot_corners(bot: &Nanobot) -> impl Iterator<Item=Point<i64>> {
    get_nanobot_grouped_corners(bot).flatten()
}

fn get_nanobot_planes(bot: &Nanobot) -> impl Iterator<Item=Plane> {
    combine(get_nanobot_grouped_corners(bot))
        .into_iter()
        .map(|c| to_tuple!(3, c.into_iter()))
        .map(|c| Fn::call(&get_plane_from_three_points, c))
}

fn is_one_of_points_in_nanobot<T: 'static + Debug + PartialEq + Copy>(
    bot: &Nanobot,
    mut points: impl Iterator<Item=Point<T>>
) -> bool
    where i64: CommonNumericType<T> + AsPrimitive<<i64 as CommonNumericType<T>>::CommonType>,
          T: AsPrimitive<<i64 as CommonNumericType<T>>::CommonType> + Zero {
    points.any(|p| is_in_range(bot, &(p, T::zero())))
}

fn intersect_full_pierced(
    cuboid_plane: &Plane,
    nanobot_plane: &Plane,
    center: &Point<i64>
) -> Point<f64> {
    let center_float: Point<f64> = convert_vec(center.clone_owned());
    let line = get_planes_intersection_line(cuboid_plane, nanobot_plane).unwrap();

    line_and_point_intersection(&line, &center_float)
}

fn cuboid_and_bot_edges_intersects(cuboid: &Cuboid, (center, radius): &Nanobot) -> bool {
    get_cuboid_lines(cuboid).any(|line|{
        let intersection = line_and_point_intersection(&line, center);
        let distance_from_center = manhattan_distance(&intersection, center);

        is_one_of_points_in_cuboid(cuboid, std::iter::once(intersection))
            && distance_from_center <= (*radius) as f64
    })
}

fn cuboid_and_bot_planes_intersects(cuboid: &Cuboid, bot@(center, radius): &Nanobot) -> bool {
    get_cuboid_planes(cuboid).any(|cuboid_plane|{
        get_nanobot_planes(bot)
            .any(|bot_plane|{
                let intersection = intersect_full_pierced(&cuboid_plane, &bot_plane, center);
                let distance_from_center = manhattan_distance(&intersection, center);

                is_one_of_points_in_cuboid(cuboid, std::iter::once(intersection))
                    && distance_from_center <= (*radius) as f64
            })
    })
}

fn intersects(cuboid: &Cuboid, bot: &Nanobot) -> bool {
        is_one_of_points_in_nanobot(bot, get_cuboid_corners(cuboid))
    ||  is_one_of_points_in_cuboid(cuboid, get_nanobot_corners(bot))
    ||  cuboid_and_bot_edges_intersects(cuboid, bot)
    ||  cuboid_and_bot_planes_intersects(cuboid, bot)
}

fn part2() {
    let clique = get_largest_clique();
    let min_max_coords = (0..3).filter_map(|i|
        if let MinMax((l, _), (r, _)) = clique
            .iter()
            .minmax_by_key(|(p, _)| p[i]) {
            Some((l[i], r[i]))
        } else { None }
    ).collect_vec();

    let mins = min_max_coords.iter().map(|(min, _)| *min);
    let radii = min_max_coords.iter().map(|(l, r)| (r - l).abs());
    let cuboid = (Point::from_iterator(mins), Radii::from_iterator(radii));

    let planes_map = clique
        .into_iter()
        .map(|c@(n, r)| (n, (get_nanobot_planes(&c), r)))
        .collect::<HashMap<_, _>>();

    let mut cuboids = BinaryHeap::<CuboidWrapper>::new();
    let mut visited = HashSet::<Cuboid>::new();
    let mut results = BinaryHeap::<CuboidWrapper>::new();

    cuboids.push(CuboidWrapper(cuboid));

    while let Some(CuboidWrapper(current_cuboid@(_, radii))) = cuboids.pop() {
        if !visited.insert(current_cuboid.clone()) {
            continue;
        } else if planes_map
            .iter()
            .all(|(p,(_, r))| intersects(&current_cuboid, &(*p, *r))) {

            if radii == Radii::zero() {
                results.push(CuboidWrapper(current_cuboid));
                continue;
            }

            for next_cuboid in bisect_cuboid(&current_cuboid) {
                cuboids.push(CuboidWrapper(next_cuboid))
            }
        }
    }

    let result =
        results
            .pop()
            .map(|CuboidWrapper((p, _))| manhattan_distance(&p, &Point::<i64>::zero()));

    println!("part 2: {:?}", result)
}

fn main() {
    part1();
    part2();
}