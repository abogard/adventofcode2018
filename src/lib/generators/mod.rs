use std::ops::{Generator, GeneratorState};
use std::pin::Pin;

pub struct GeneratorIter<T, S>(Pin<Box<S>>) where S: Generator<Return = (), Yield = T>;

impl<T, S> GeneratorIter<T, S> where S: Generator<Return = (), Yield = T>  {
    pub fn new(generator: Box<S>) -> GeneratorIter<T, S> where S: Unpin {
        GeneratorIter(Pin::new(generator))
    }

    pub fn new_unsafe(generator: Box<S>) -> GeneratorIter<T, S> {
        unsafe { GeneratorIter(Pin::new_unchecked(generator)) }
    }
}

impl <T, S> Iterator for GeneratorIter<T, S> where S: Generator<Return = (), Yield = T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match self.0.as_mut().resume(()) {
            GeneratorState::Yielded(item) => Some(item),
            GeneratorState::Complete(_) => None,
        }
    }
}

#[macro_export]
macro_rules! gen_iter {
    ($e:expr) => (GeneratorIter::new(Box::new(|| $e)))
}

#[macro_export]
macro_rules! gen_unsafe_iter {
    ($e:expr) => (GeneratorIter::new_unsafe(Box::new(static move || $e)))
}