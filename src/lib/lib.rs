#![feature(type_ascription)]
#![feature(generator_trait)]
#![feature(box_syntax)]

pub mod graph;
pub mod utils;
pub mod geometry;
pub mod generators;