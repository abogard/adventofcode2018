use std::hash::Hash;
use rand::seq::IteratorRandom;
use rand::thread_rng;
use std::collections::HashSet;

pub type Clique<'a, T> = HashSet<&'a T>;

pub trait Graph<T: Eq + Hash> where Self: Sized {
    fn get_nodes<'a>(&'a self) -> Box<dyn Iterator<Item=&'a T> + 'a>;
    fn get_neighbours<'a>(&'a self, node: &T) -> Box<dyn Iterator<Item=&'a T> + 'a>;

    fn maximal_cliques<'a>(&'a self) -> Vec<Clique<'a, T>> {
        let possible = self.get_nodes().collect::<Clique::<'a, T>>();
        let impossible = Clique::<'a, T>::new();
        let maximal_clique = Clique::<'a, T>::new();
        let mut maximal_cliques = Vec::<Clique::<'a, T>>::new();

        bron_kerbosh_impl(self, possible, impossible, maximal_clique, &mut maximal_cliques);

        maximal_cliques
    }

    fn maximum_clique(&self) -> Clique<T> {
        self
            .maximal_cliques()
            .into_iter()
            .max_by_key(|c| c.len())
            .unwrap_or(Clique::<T>::new())
    }
}

fn bron_kerbosh_impl<'a, T, S: Eq + Hash>(
    graph: &'a T,
    mut possible: Clique::<'a, S>,
    mut impossible: Clique::<'a, S>,
    maximal_clique: Clique::<'a, S>,
    result: &mut Vec<Clique::<'a, S>>) where T: Graph<S> {

    if possible.is_empty() && impossible.is_empty() {
        result.push(maximal_clique)
    } else {
        let pivot =
            possible
                .iter()
                .chain(impossible.iter())
                .choose(&mut thread_rng());

        let pivot_neighbours =
            pivot
                .map(|p| graph.get_neighbours(*p))
                .unwrap_or(box std::iter::empty());

        let mut possible_clone = possible.clone();
        pivot_neighbours.for_each(|n| { possible_clone.remove(n); });

        for node in possible_clone {
            let reduced_possible = graph
                .get_neighbours(node)
                .filter(|n| possible.contains(*n))
                .collect::<Clique<'a, S>>();

            let reduced_impossible = graph
                .get_neighbours(node)
                .filter(|n| impossible.contains(*n))
                .collect::<Clique<'a, S>>();

            let mut updated_maximal_clique = maximal_clique.clone();
            updated_maximal_clique.insert(node);

            bron_kerbosh_impl(
                graph,
                reduced_possible,
                reduced_impossible,
                updated_maximal_clique,
                result
            );

            possible.remove(node);
            impossible.insert(node);
        }
    }
}