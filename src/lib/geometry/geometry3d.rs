use std::fmt::Debug;
use std::ops::{Sub, Add};
use std::iter::Sum;
use nalgebra::{Vector3, Matrix3, RowVector3};
use num_traits::{AsPrimitive, Signed, Zero};
use itertools::Itertools;
use crate::utils::combine;

pub type Point<T> = Vector3<T>;
pub type Parameters<T> = Vector3<T>;
pub type Line<T> = (Point<T>, Parameters<T>); //Parametric equation of a line in 3d space
pub type IPoint = Point<i64>;
pub type Radius = i64;
pub type Radii = Vector3<Radius>;
pub type Normal = IPoint;
pub type Plane = (Normal, i64); // Normal plane equation Ax + By + Cz = D
pub type Cuboid = (IPoint, Radii); // Most bottom-left corner and lengths of edges

pub trait CommonNumericType<T> {
    type CommonType: Copy + Debug + PartialEq + PartialOrd + Sub + Add + Sum + Signed;
}

impl CommonNumericType<f64> for i64 { type CommonType = f64; }
impl CommonNumericType<i64> for f64 { type CommonType = f64; }
impl CommonNumericType<f64> for f64 { type CommonType = f64; }
impl CommonNumericType<i64> for i64 { type CommonType = i64; }

pub fn manhattan_distance<T: 'static + Debug + PartialEq, S: 'static + Debug + PartialEq>(
    p1: &Vector3<T>,
    p2: &Vector3<S>
) -> <T as CommonNumericType<S>>::CommonType
    where T: CommonNumericType<S> + AsPrimitive<<T as CommonNumericType<S>>::CommonType>,
          S: AsPrimitive<T::CommonType> {
    p1
        .iter()
        .zip(p2.iter())
        .map(|(l, r)| (l.as_() - r.as_()).abs())
        .sum()
}

pub fn get_cuboid_corners((point, radii): &Cuboid) -> impl Iterator<Item=IPoint> {
    let coords = (0..point.len())
        .map(|i| vec![point[i], point[i] + radii[i]].into_iter());

    combine(coords)
        .into_iter()
        .map(Vector3::from_vec)
}

pub fn is_one_of_points_in_cuboid<T> (
    (point, radii): &Cuboid,
    mut points: impl Iterator<Item=Point<T>>
) -> bool where
    T: 'static + Debug + PartialEq + PartialOrd + Copy + AsPrimitive<T>,
    i64: AsPrimitive<T> {
    points.any(|p|
        p.iter()
            .zip(point.iter().zip(radii.iter()))
            .all(|(&i, (&j, &r))| j.as_() <= i && i <= (j + r).as_())
    )
}

pub fn get_cuboid_lines(cuboid: &Cuboid) -> impl Iterator<Item=Line<i64>> {
    get_cuboid_corners(cuboid)
        .combinations(2)
        .filter(|v| v[0]
            .iter()
            .zip(v[1].iter())
            .filter(|(i, j)| i != j)
            .count() == 1
        ).map(|v| (v[1] - v[0], v[0]))
}

pub fn get_cuboid_planes<'a>((point, radii): &'a Cuboid) -> impl Iterator<Item=Plane> + 'a {
    let diagonal = Matrix3::<i64>::identity()
        .column_iter()
        .map(|c| c.clone_owned())
        .collect_vec();

    point
        .iter()
        .zip(radii.iter())
        .zip(diagonal.into_iter())
        .flat_map(|((p, r), n)| vec![(n.clone_owned(), -*p), (n.clone_owned(), -(p + r))])
}

pub fn bisect_cuboid(&(edge, radii): &Cuboid) -> impl Iterator<Item=Cuboid> {
    let reduced = radii / 2;
    combine(edge
        .iter()
        .enumerate()
        .map(|(i, e)| vec![*e, *e + radii[i] - reduced[i]].into_iter())
    ).into_iter()
        .map(move |e| (Vector3::from_vec(e), reduced.clone_owned()))
}

pub fn line_and_point_intersection<T: 'static + Debug + PartialEq, S: 'static + Debug + PartialEq>(
    (params, start): &Line<T>,
    point: &Point<S>
) -> Point<f64>
    where T: AsPrimitive<f64>,
          S: AsPrimitive<f64> {

    let start_normalized: Vector3<f64> = convert_vec(*start);
    let point_normalized: Vector3<f64> = convert_vec(*point);
    let params_normalized: Vector3<f64> = convert_vec(*params);

    let diff = start_normalized - point_normalized;
    let t = -(params_normalized.dot(&diff) / params_normalized.dot(&params_normalized));

    start_normalized + t * params_normalized
}
pub fn convert_vec<T: 'static + Debug + PartialEq, S: 'static + Debug + PartialEq + Copy>(
    vec: Vector3<T>
) -> Vector3<S> where T: AsPrimitive<S> {
    Vector3::from_iterator(vec.into_iter().map(|c| c.as_()))
}

pub fn get_plane_from_three_points(point1: IPoint, point2: IPoint, point3: IPoint) -> Plane {
    let l = point2 - point1;
    let r = point3 - point1;
    let normal = l.cross(&r);
    let d = point1
        .iter()
        .zip(normal.iter())
        .map(|(d1, d2)| d1 * d2)
        .sum::<i64>();

    (normal, -d)
}

pub fn get_planes_intersection_line(
    &(normal1, d1): &Plane,
    &(normal2, d2): &Plane,
) -> Option<Line<f64>> {
    let f_normal1 = (convert_vec(normal1): Vector3<f64>).transpose();
    let f_normal2  = (convert_vec(normal2): Vector3<f64>).transpose();
    let mut equation = Matrix3::<f64>::identity();
    let parameter_index = f_normal1
        .iter()
        .enumerate()
        .chain(f_normal2.iter().enumerate())
        .find(|&(_, e)| *e == 0.0)
        .map(|(i, _)| i)
        .unwrap_or(2);

    let mut additional_equation = RowVector3::zero();
    additional_equation[parameter_index] = 1.0;

    equation.set_row(0, &f_normal1);
    equation.set_row(1, &f_normal2);
    equation.set_row(2, &additional_equation);

    let lu = equation.lu();
    let b1 = Vector3::<f64>::new(d1 as f64, d2 as f64, 1.);
    let b2 = Vector3::<f64>::new(d1 as f64, d2 as f64, 2.);

    let solution1 = lu.solve(&b1);
    let solution2 = lu.solve(&b2);

    solution1
        .iter()
        .zip(solution2.iter())
        .map(|(a, b)| (a.clone_owned(), b - a))
        .next()
}