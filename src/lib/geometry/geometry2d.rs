use std::cmp::Ordering;
use std::ops::Add;

#[derive(Eq, PartialEq, Hash, Clone, Debug, Copy)]
pub struct Point(pub i64, pub i64);

impl Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Ord for Point {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.1, self.0).cmp(&(other.1, other.0))
    }
}

impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}