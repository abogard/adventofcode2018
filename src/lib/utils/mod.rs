use std::fs::File;
use std::path::Path;
use std::io::{self, BufRead, BufReader};
use itertools::Itertools;

pub fn get_buff_reader(path: &str) -> BufReader<File> {
    let error = format!("Filed {:?} can't be opened!", path);
    let file = File::open(Path::new(path)).expect(error.as_str());

    io::BufReader::new(file)
}

pub fn get_file_contents(path: &str) -> impl Iterator<Item=String> {
    get_buff_reader(path)
        .lines()
        .map(|line| line.expect("Can't read line!"))
}

#[macro_export]
macro_rules! eq_or {
    ($left: expr, $right: expr) => ($left == $right);
    ($left: expr, $right: expr, $($next: expr),+) => (
        ($left == $right) || eq_or!($left, $($next),+)
    )
}

pub fn combine<T: Clone>(input: impl Iterator<Item=impl Iterator<Item=T>>) -> Vec<Vec<T>> {
    input
        .fold(vec![Vec::<T>::new()], |acc, item| {
            item
                .cartesian_product(acc.iter())
                .map(|(l, r)| {
                    let mut cloned = r.clone();
                    cloned.push(l.clone());
                    cloned
                }).collect_vec()
        })
}

pub mod options {
    pub fn append<T, S>(option: Option<T>, to_append: S) -> Option<(T, S)> {
        option.map(|item| (item, to_append))
    }

    pub fn prepend<T, S>(option: Option<T>, to_prepend: S) -> Option<(S, T)> {
        option.map(|item| (to_prepend, item))
    }
}

pub mod results {
    pub fn append<T, S, E>(result: Result<T, E>, to_append: S) -> Result<(T, S), E> {
        result.map(|item| (item, to_append))
    }

    pub fn prepend<T, S, E>(result: Result<T, E>, to_prepend: S) -> Result<(S, T), E> {
        result.map(|item| (to_prepend, item))
    }
}