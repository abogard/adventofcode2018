#![feature(bindings_after_at, map_first_last, fn_traits)]

use std::collections::{HashMap, BTreeMap};
use owned_chars::OwnedCharsExt;
use std::convert::{TryInto, TryFrom};
use itertools::Itertools;
use std::ops::Add;

#[macro_use]
extern crate lazy_static;

use tools::utils::{get_file_contents, options::prepend};

const FILE_PATH: &str = "resources/day13/part1.txt";
const UP: Point = Point(0, -1);
const DOWN: Point = Point(0, 1);
const LEFT: Point = Point(-1, 0);
const RIGHT: Point = Point(1, 0);

lazy_static! {
    static ref TRANSITIONS: Vec<fn(Point) -> Point> = vec![
        |Point(x, y)| Point(y, -1*x),
        |xy| xy,
        |Point(x, y)| Point(-1*y, x)
    ];
}

#[derive(Eq, Ord, PartialOrd, PartialEq, Hash, Copy, Clone, Debug)]
struct Point(i64, i64);

impl TryFrom<char> for Point {
    type Error = String;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '^' => Ok(UP),
            'v' => Ok(DOWN),
            '<' => Ok(LEFT),
            '>' => Ok(RIGHT),
            _ => Err(format!("{} can't be converted to Direction!", value))
        }
    }
}

impl Into<Point> for (i64, i64) {
    fn into(self) -> Point {
        Point(self.0, self.1)
    }
}

impl Add for Point  {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point(self.0 + rhs.0, self.1 + rhs.1)
    }
}

type Board = HashMap<Point, char>;
type Turning  = u8;
type CartMeta = (Point, Turning);
type CartData = (Point, CartMeta);
type OrderedCarts = BTreeMap<Point, CartMeta>;

fn get_board_row_chars(y: usize, line: String) -> impl Iterator<Item=(Point, char)> {
    line
        .into_chars()
        .enumerate()
        .map(move |(x, c)| (Point(x as i64, y as i64), c))
}

fn read_tracks() -> Board {
    get_file_contents(FILE_PATH)
        .enumerate()
        .map(|arg| Fn::call(&get_board_row_chars, arg))
        .flatten()
        .collect::<Board>()
}

fn move_cart((point, (direction@Point(x, y), turning)): CartData, track: &Board) -> CartData {
    let next_position = point + direction;
    let next_data = match track.get(&next_position).unwrap() {
        '/'  => (Point(-1 * y, -1 * x), turning),
        '\\' => (Point(y, x), turning),
        '+'  => (TRANSITIONS[turning as usize](direction), (turning + 1) % TRANSITIONS.len() as u8),
        _    => (direction, turning)
    };

    (next_position, next_data)
}

fn get_track_and_carts() -> (Board, BTreeMap<Point, CartMeta>) {
    let tracks = read_tracks();
    let carts =
        tracks
            .iter()
            .filter_map(|(&k, &c)| prepend(TryInto::<Point>::try_into(c).ok(), k))
            .map(|(k, v)| (k, (v, 0u8)))
            .collect::<OrderedCarts>();

    (tracks, carts)
}

fn simulate_crash(
    track: &Board,
    mut carts: OrderedCarts,
    carts_till_stop: usize
) -> (Point, impl Iterator<Item=Point>) {
    loop {
        let mut old_carts = carts;
        carts = OrderedCarts::new();

        while let Some(cart_data) = old_carts.pop_first() {
            let (pos, data) = move_cart(cart_data, &track);

            if old_carts.contains_key(&pos) || carts.contains_key(&pos) {
                carts.remove(&pos);
                old_carts.remove(&pos);

                if carts.len() + old_carts.len() <= carts_till_stop {
                    let carts_keys = carts.into_iter().map(|(k, _)| k);
                    let old_carts_keys = old_carts.into_iter().map(|(k, _)| k);

                    return (pos, carts_keys.merge(old_carts_keys));
                }
            } else {
                carts.insert(pos, data);
            }
        }
    }
}

fn part1() {
    let (track, carts) = get_track_and_carts();
    let carts_count = carts.len();
    let (crash_position, _) = simulate_crash(&track, carts, carts_count - 1);

    println!("part 1: {:?}", crash_position);
}

fn part2() {
    let (track, carts) = get_track_and_carts();
    let (_, mut last_cart) = simulate_crash(&track, carts, 1);

    println!("part 2: {:?}", last_cart.next());
}

fn main() {
    part1();
    part2();
}