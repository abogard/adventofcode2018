#![feature(type_alias_impl_trait)]

#[macro_use]
extern crate strum_macros;

use std::collections::HashMap;

use itertools::Itertools;
use once_cell::sync::Lazy;
use regex::Regex;

use OperationType::*;
use macros::regex;
use tools::utils::get_file_contents;

const REGISTRIES: usize = 6;
const FILE_PATH_PART_1: &'static str = "resources/day19/part1.txt";

type Registry = [usize; REGISTRIES];

const REGISTRY_REGEX: Lazy<Regex> = regex!(r"#ip (\d+)");
const INSTRUCTION_REGEX: Lazy<Regex> = regex!(r"(\p{Alpha}+) (\d+) (\d+) (\d+)");
static OPERATIONS: Lazy<HashMap<String, fn(&mut Registry, &Operation)>> = Lazy::new(||{
    let mut hash_map = HashMap::<OperationType, fn(&mut Registry, &Operation)>::new();

    hash_map.insert(ADDR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) + r.g(b));
    hash_map.insert(ADDI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) + b);
    hash_map.insert(MULR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) * r.g(b));
    hash_map.insert(MULI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) * b);
    hash_map.insert(BANR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) & r.g(b));
    hash_map.insert(BANI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) & b);
    hash_map.insert(BORR, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) | r.g(b));
    hash_map.insert(BORI, |r, &Operation{a, b, c}| *r.s(c) = r.g(a) | b);
    hash_map.insert(SETR, |r, &Operation{a, b: _b, c}| *r.s(c) = r.g(a));
    hash_map.insert(SETI, |r, &Operation{a, b: _b, c}| *r.s(c) = a);
    hash_map.insert(GTIR, |r, &Operation{a, b, c}| *r.s(c) = if a > r.g(b) { 1 } else { 0 });
    hash_map.insert(GTRI, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) > b { 1 } else { 0 });
    hash_map.insert(GTRR, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) > r.g(b) { 1 } else { 0 });
    hash_map.insert(EQIR, |r, &Operation{a, b, c}| *r.s(c) = if a == r.g(b) { 1 } else { 0 });
    hash_map.insert(EQRI, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) == b { 1 } else { 0 });
    hash_map.insert(EQRR, |r, &Operation{a, b, c}| *r.s(c) = if r.g(a) == r.g(b) { 1 } else { 0 });

    hash_map
        .into_iter()
        .map(|(k, v)| (k.as_ref().to_lowercase(), v))
        .collect::<HashMap<_, _>>()
});

#[derive(Debug, Hash, Eq, PartialEq, Ord, PartialOrd, Copy, Clone, AsRefStr)]
enum OperationType {
    ADDR,
    ADDI,
    MULR,
    MULI,
    BANR,
    BANI,
    BORR,
    BORI,
    SETR,
    SETI,
    GTIR,
    GTRI,
    GTRR,
    EQIR,
    EQRI,
    EQRR
}

#[derive(Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
struct Operation {
    a: usize,
    b: usize,
    c: usize,
}

trait RegistryActions {
    fn s(&mut self, reg: usize) -> &mut usize;
    fn g(&self, reg: usize) -> usize;
}

impl RegistryActions for Registry {
    fn s(&mut self, reg: usize) -> &mut usize {
        &mut self[reg]
    }
    fn g(&self, reg: usize) -> usize {
        self[reg]
    }
}

type IP = usize;
type Action = impl Fn(&mut [usize;6]);

fn read_instructions() -> (IP, Vec<Action>) {
    let mut lines = get_file_contents(FILE_PATH_PART_1);

    if let Some(ip) = lines.next() {
        if let Some(ip_start) = REGISTRY_REGEX
            .captures(&ip)
            .filter(|m| m.len() > 1)
            .and_then(|m| m[1].parse::<usize>().ok()) {

            return (ip_start, lines.flat_map(|line| INSTRUCTION_REGEX
                .captures(&line)
                .filter(|m| m.len() > 4)
                .and_then(|m| {
                    let op = Operation {
                        a: m[2].parse().unwrap(),
                        b: m[3].parse().unwrap(),
                        c: m[4].parse().unwrap()
                    };

                    OPERATIONS
                        .get(&m[1])
                        .map(|action| move |reg: &mut [usize;6]| action(reg, &op))
                })
            ).collect_vec())
        }
    }
    panic!("Incorrect registry data");
}

fn run_program(mut reg: [usize; REGISTRIES]) -> [usize; 6] {
    let (bound, op) = read_instructions();
    let mut ip = 0;

    while ip < op.len() {
        reg[bound] = ip;
        op[ip](&mut reg);
        ip = if let Some(&r) = reg.get(bound) { r + 1 } else { break; }
    }

    reg
}

fn part1() {
    println!("part 1: {}", run_program([0usize; REGISTRIES])[0]);
}

fn part2() {
    let number = 10551306usize;
    let divisors_sum =
        (1..=number)
            .rev()
            .filter(|&d| number % d == 0)
            .map(|d| number / d)
            .sum::<usize>();

    let reg = [divisors_sum, 7, 1, 1, number, number];

    println!("part 2: {}", run_program(reg)[0]);
}

fn main() {
    part1();
    part2();
}