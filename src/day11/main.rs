#![feature(exact_size_is_empty, const_generics)]

use itertools::{unfold, Itertools};
use rayon::prelude::*;
use std::slice::Windows;

use tools::utils::options::prepend;

const GRID_DIMENSION_X: usize = 300;
const GRID_DIMENSION_Y: usize = 300;
const GRID_SERIAL_NUMBER: usize = 7989;

type Point = (usize, usize);
type Power = i32;

fn zip_windows<'a, const N: usize>(
    slices: &'a [[i32; N]],
    windows_size: usize
) -> impl Iterator<Item=i32> + 'a {
    let windows = slices
        .iter()
        .map(|x|x.windows(windows_size))
        .collect_vec();

    unfold(windows, |iters|{
        if iters.iter().any(Windows::is_empty) {
            None
        } else {
            Some(iters
                .iter_mut()
                .flat_map(Windows::next)
                .map(|iter| iter.iter().sum::<i32>())
                .sum::<i32>()
            )
        }
    })
}

fn calculate_grid() -> [[Power; GRID_DIMENSION_X]; GRID_DIMENSION_Y] {
    let mut grid = [[0i32; GRID_DIMENSION_X]; GRID_DIMENSION_Y];

    for y in 0..GRID_DIMENSION_Y {
        for x in 0..GRID_DIMENSION_X {
            let rack_id = x + 1 + 10;
            let power_level = (rack_id * (y + 1) + GRID_SERIAL_NUMBER) * rack_id;
            let reduced = power_level % 1000;
            let one_hundreds = (reduced / 100) as Power ;
            grid[y][x] = one_hundreds - 5
        }
    }

    grid
}

fn find_highest_fuel_grid<const X: usize, const Y: usize>(
    grid: &[[Power; X]; Y],
    sub_grid_size: usize
) -> Option<(Point, Power)> {
    grid
        .windows(sub_grid_size)
        .enumerate()
        .flat_map(|(j, slice_y)|
            zip_windows(slice_y, sub_grid_size)
                .enumerate()
                .map(|(i, sum)| ((i + 1, j + 1), sum))
                .max_by_key(|&(_, s)| s)
        ).max_by_key(|&(_, s)| s)
}

fn part_1() {
    let result = find_highest_fuel_grid(&calculate_grid(), 3);

    println!("part 1: {:?}", result);
}

fn part_2() {
    let grid = calculate_grid();
    let result = (1usize..=300usize)
        .collect_vec()
        .par_iter()
        .map(|&size| prepend(find_highest_fuel_grid(&grid, size), size))
        .flatten()
        .max_by_key(|&(_, (_, s))| s);

    println!("part 2: {:?}", result);
}

fn main() {
    part_1();
    part_2();
}

