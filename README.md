# AdventOfCode2018

Solutions for Advent Of Code 2018 coding competition written in Rust

The code has been tested on rustc 1.46.0 nightly 

*To run an exercise from a specific day X type "cargo run --release --bin dayX"*
