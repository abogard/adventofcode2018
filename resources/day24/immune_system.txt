7056 units each with 8028 hit points (weak to radiation) with an attack that does 10 slashing damage at initiative 13
4459 units each with 10339 hit points (immune to fire, radiation, slashing) with an attack that does 22 cold damage at initiative 4
724 units each with 10689 hit points (immune to bludgeoning, cold, fire) with an attack that does 124 radiation damage at initiative 17
1889 units each with 3361 hit points (weak to cold) with an attack that does 17 fire damage at initiative 2
4655 units each with 1499 hit points (weak to fire) with an attack that does 2 fire damage at initiative 5
6799 units each with 3314 hit points with an attack that does 4 radiation damage at initiative 16
2407 units each with 4016 hit points (weak to slashing; immune to bludgeoning) with an attack that does 13 fire damage at initiative 20
5372 units each with 5729 hit points with an attack that does 9 fire damage at initiative 14
432 units each with 11056 hit points with an attack that does 220 cold damage at initiative 10
3192 units each with 8960 hit points (weak to slashing, radiation) with an attack that does 24 cold damage at initiative 15