4052 units each with 25687 hit points (weak to fire, radiation) with an attack that does 11 slashing damage at initiative 18
1038 units each with 13648 hit points (weak to slashing) with an attack that does 24 bludgeoning damage at initiative 9
6627 units each with 34156 hit points (weak to radiation) with an attack that does 10 slashing damage at initiative 6
2299 units each with 45224 hit points (weak to fire) with an attack that does 38 cold damage at initiative 19
2913 units each with 30594 hit points (weak to radiation; immune to cold) with an attack that does 20 fire damage at initiative 1
2153 units each with 14838 hit points (immune to fire, bludgeoning, radiation; weak to slashing) with an attack that does 11 radiation damage at initiative 3
2381 units each with 61130 hit points (weak to cold) with an attack that does 39 slashing damage at initiative 8
2729 units each with 33834 hit points (immune to slashing, cold) with an attack that does 23 fire damage at initiative 7
344 units each with 20830 hit points (immune to fire) with an attack that does 116 bludgeoning damage at initiative 12
6848 units each with 50757 hit points with an attack that does 12 slashing damage at initiative 11