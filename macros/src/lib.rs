#![feature(in_band_lifetimes)]
extern crate proc_macro;

use proc_macro::TokenStream;
use itertools::Itertools;
use std::iter::repeat;
use syn;
use syn::{Result, Error, parse_macro_input};
use syn::parse::{Parse, ParseBuffer};
use quote::quote;

use regex::Regex;

#[proc_macro]
pub fn to_fixed_size_array(item: TokenStream) -> TokenStream {
    expand_iterators(item, |s| format!("[{}]", s))
}

#[proc_macro]
pub fn to_tuple(item: TokenStream) -> TokenStream {
    expand_iterators(item, |s| format!("({})", s))
}

#[proc_macro]
pub fn regex(item: TokenStream) -> TokenStream {
    let RegexPattern { pattern } = parse_macro_input!(item as RegexPattern);

    Regex::new(&pattern.value()).unwrap();

    let result = quote! {
        Lazy::new(|| Regex::new(#pattern).unwrap())
    };

    result.into()
}

fn expand_iterators(item: TokenStream, wrapper: impl FnOnce(String) -> String) -> TokenStream {
    let arg = item
        .into_iter()
        .map(|t| t.to_string())
        .collect_vec();

    match &arg[..] {
        [count_str, sep, iter_str @ ..] if sep == "," && count_str.parse::<usize>().is_ok() => {
            let count = count_str.parse::<usize>().unwrap();
            let expr = iter_str.join("");
            let elements = repeat("x.next().unwrap()").take(count).join(",");

            format!("{{ let mut x = {}; {}}}", expr, wrapper(elements)).parse().unwrap()
        },
        [_, _] => panic!("The first argument must be of usize type"),
        _ => panic!("This macro takes exactly two arguments!")
    }
}

struct RegexPattern {
    pattern: syn::LitStr
}

impl Parse for RegexPattern {
    fn parse(input: &'a ParseBuffer<'a>) -> Result<Self> {
        let pattern = input.parse::<syn::LitStr>()?;

        if input.is_empty() {
            Ok(RegexPattern { pattern })
        } else {
            Err(Error::new(input.span(), "Macro regex! takes exactly one argument!!"))
        }
    }
}
